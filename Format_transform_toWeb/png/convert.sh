

# for file in *.pdf; do

# # echo $file

#    convert -density 800 $file $file.jpg

# done

convert -density 500x500 -quality 100 +append a4j_timel.pdf a4j_timel_b.pdf a4j_merged.png
convert -density 500x500 -quality 100 +append apache_cli_timel.pdf apache_cli_timel_b.pdf apache_cli_merged.png
convert -density 500x500 -quality 100 +append apache_codec_timel.pdf apache_codec_timel_b.pdf apache_codec_merged.png
convert -density 500x500 -quality 100 +append apache_coll_timel.pdf apache_coll_timel_b.pdf apache_coll_merged.png
convert -density 500x500 -quality 100 +append apache_comp_timel.pdf apache_comp_timel_b.pdf apache_comp_merged.png
convert -density 500x500 -quality 100 +append apache_math_timel.pdf apache_math_timel_b.pdf apache_math_merged.png
convert -density 500x500 -quality 100 +append apache_prim_timel.pdf apache_prim_timel_b.pdf apache_prim_merged.png
convert -density 500x500 -quality 100 +append apache_shiro_timel.pdf apache_shiro_timel_b.pdf apache_shiro_merged.png
convert -density 500x500 -quality 100 +append ASM5.0.1_timel.pdf ASM5.0.1_timel_b.pdf ASM5.0.1_merged.png
convert -density 500x500 -quality 100 +append bcel_timel.pdf bcel_timel_b.pdf bcel_merged.png
convert -density 500x500 -quality 100 +append beanbin_timel.pdf beanbin_timel_b.pdf beanbin_merged.png
convert -density 500x500 -quality 100 +append classviewer_timel.pdf classviewer_timel_b.pdf classviewer_merged.png
convert -density 500x500 -quality 100 +append easymock_timel.pdf easymock_timel_b.pdf easymock_merged.png
convert -density 500x500 -quality 100 +append fixsuite_timel.pdf fixsuite_timel_b.pdf fixsuite_merged.png
convert -density 500x500 -quality 100 +append follow_timel.pdf follow_timel_b.pdf follow_merged.png
convert -density 500x500 -quality 100 +append Guava_timel.pdf Guava_timel_b.pdf Guava_merged.png
convert -density 500x500 -quality 100 +append javassit_timel.pdf javassit_timel_b.pdf javassit_merged.png
convert -density 500x500 -quality 100 +append javaviewcontrol_timel.pdf javaviewcontrol_timel_b.pdf javaviewcontrol_merged.png
convert -density 500x500 -quality 100 +append javax_mail_timel.pdf javax_mail_timel_b.pdf javax_mail_merged.png
convert -density 500x500 -quality 100 +append jaxen_timel.pdf jaxen_timel_b.pdf jaxen_merged.png
convert -density 500x500 -quality 100 +append jcommander_timel.pdf jcommander_timel_b.pdf jcommander_merged.png
convert -density 500x500 -quality 100 +append jdom1.0_timel.pdf jdom1.0_timel_b.pdf jdom1.0_merged.png
convert -density 500x500 -quality 100 +append joda_time_timel.pdf joda_time_timel_b.pdf joda_time_merged.png
convert -density 500x500 -quality 100 +append JSAP_timel.pdf JSAP_timel_b.pdf JSAP_merged.png
convert -density 500x500 -quality 100 +append Mango_timel.pdf Mango_timel_b.pdf Mango_merged.png
convert -density 500x500 -quality 100 +append mockito-core_timel.pdf mockito-core_timel_b.pdf mockito-core_merged.png
convert -density 500x500 -quality 100 +append nekomud_timel.pdf nekomud_timel_b.pdf nekomud_merged.png
convert -density 500x500 -quality 100 +append SAT4J_core_timel.pdf SAT4J_core_timel_b.pdf SAT4J_core_merged.png
convert -density 500x500 -quality 100 +append scch_timel.pdf scch_timel_b.pdf scch_merged.png
convert -density 500x500 -quality 100 +append TinySql_timel.pdf TinySql_timel_b.pdf TinySql_merged.png
convert -density 500x500 -quality 100 +append total_scov_600s.pdf total_bcov_600s.pdf total_cov_600s_merged.png
convert -density 500x500 -quality 100 +append total_cov_3000s.pdf total_cov_3000s.pdf.png
