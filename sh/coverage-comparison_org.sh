#!/bin/sh
# compare total coverage at end of each run

[ -e cov_data ] || mkdir cov_data

# 3000s x 2
find . -name 'cov*.txt' | \
	grep 3000 | \
	grep -i default | \
	sed -e 's/\(.*\)/tail -1 \1/' | \
	sh | \
	sed -e 's/,/ /' \
> cov_data/3000s_default_all.dat

find . -name 'cov*.txt' | \
	grep 3000 | \
	grep -i enhancement | \
	sed -e 's/\(.*\)/tail -1 \1/' | \
	sh | \
	sed -e 's/,/ /' \
> cov_data/3000s_enhanced_all.dat

find . -name 'cov*.txt' | \
	grep 3000 | \
	grep -i evosuite | \
	sed -e 's/\(.*\)/tail -1 \1/' | \
	sh | \
	sed -e 's/,/ /' \
> cov_data/3000s_evosuite_all.dat


# 600s x 8
find . -name 'cov*.txt' | \
	grep 600 | \
	grep -i default | \
	sed -e 's/\(.*\)/tail -1 \1/' | \
	sh | \
	sed -e 's/,/ /' \
> cov_data/600s_def_all.dat

find . -name 'cov*.txt' | \
	grep 600 | \
	grep -i 1.cons | \
	sed -e 's/\(.*\)/tail -1 \1/' | \
	sh | \
	sed -e 's/,/ /' \
> cov_data/600s_con_all.dat

find . -name 'cov*.txt' | \
	grep 600 | \
	grep -i 2.orient | \
	sed -e 's/\(.*\)/tail -1 \1/' | \
	sh | \
	sed -e 's/,/ /' \
> cov_data/600s_ori_all.dat

find . -name 'cov*.txt' | \
	grep 600 | \
	grep -i 3.elep | \
	sed -e 's/\(.*\)/tail -1 \1/' | \
	sh | \
	sed -e 's/,/ /' \
> cov_data/600s_ele_all.dat

find . -name 'cov*.txt' | \
	grep 600 | \
	grep -i 4.impu | \
	sed -e 's/\(.*\)/tail -1 \1/' | \
	sh | \
	sed -e 's/,/ /' \
> cov_data/600s_imp_all.dat

find . -name 'cov*.txt' | \
	grep 600 | \
	grep -i 5.dete | \
	sed -e 's/\(.*\)/tail -1 \1/' | \
	sh | \
	sed -e 's/,/ /' \
> cov_data/600s_det_all.dat

find . -name 'cov*.txt' | \
	grep 600 | \
	grep -i 6.bloo | \
	sed -e 's/\(.*\)/tail -1 \1/' | \
	sh | \
	sed -e 's/,/ /' \
> cov_data/600s_blo_all.dat

find . -name 'cov*.txt' | \
	grep 600 | \
	grep -i combined | \
	sed -e 's/\(.*\)/tail -1 \1/' | \
	sh | \
	sed -e 's/,/ /' \
> cov_data/600s_com_all.dat

cd cov_data; \
./plot.sh
