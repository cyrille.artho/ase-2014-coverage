#!/bin/sh

gnuplot < plot.gnuplot

for F in *.ps
do
	ps2epsi $F
	B="`echo $F | sed -e 's/\.ps$//'`"
	mv $B.eps{i,}
	epstopdf $B.eps
done
