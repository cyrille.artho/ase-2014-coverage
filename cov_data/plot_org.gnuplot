set terminal postscript eps enhanced mono "Helvetica" 18
set out "total_cov_3000s.ps"

set border 2 front linetype -1 linewidth 1
set style fill solid 0.1 border lt -1
set style boxplot outliers pointtype 7
set style data boxplot
set boxwidth 0.5
set pointsize 2

unset key
set grid ytics
set xtics border in scale 0,0 nomirror norotate offset character 0, 0, 0 autojustify
set xtics norangelimit

set xtics ("\nRan." 1.0, \
"Instr. cov.\nGRT" 2.0, \
"\nEvo." 3.0, \
"\nRan." 4.0, \
"Branch cov.\nGRT" 5.0, \
"\nEvo." 6.0, \
"\nRan." 7.0, \
"Mutation cov.\nGRT" 8.0, \
"\nEvo." 9.0)

set ytics border in scale 1,0.5 nomirror norotate  offset character 0, 0, 0 autojustify
set ylabel "Coverage"
set bars 2.0

set xrange [0:10]
set yrange [0.0:1.0]

plot '3000s_default_all.dat' using (1):2 lw 3, \
'3000s_enhanced_all.dat' using (2):2 ls 1 lw 3, \
'3000s_default_all.dat' using (4):3 ls 1 lw 3, \
'3000s_enhanced_all.dat' using (5):3 ls 1 lw 3

# Add:
# '3000s_evosuite_all.dat' using (3):X lw 3, \
# '3000s_evosuite_all.dat' using (6):Y lw 3, \
# '3000s_default_mut.dat' using (7):X lw 3, \
# '3000s_enhanced_mut.dat' using (8):X lw 3, \
# '3000s_evosuite_mut.dat' using (9):X lw 3

# Add \ at end of line for item (5)
# be careful to use the right column (X, Y)


set out "total_scov_600s.ps"

set ylabel "Instruction coverage"

set xtics ("Default" 1.0, "\nConstant Mining" 2.0, "Impurity" 3.0, "\nElephant Brain" 4.0, "Detective" 5.0, "\nOrienteering" 6.0, "Bloodhound" 7.0, "\nCombined" 8.0)

plot '600s_def_all.dat' using (1):2 lw 3, \
'600s_con_all.dat' using (2):2 ls 1 lw 3, \
'600s_imp_all.dat' using (3):2 ls 1 lw 3, \
'600s_ele_all.dat' using (4):2 ls 1 lw 3, \
'600s_det_all.dat' using (5):2 ls 1 lw 3, \
'600s_ori_all.dat' using (6):2 ls 1 lw 3, \
'600s_blo_all.dat' using (7):2 ls 1 lw 3, \
'600s_com_all.dat' using (8):2 ls 1 lw 3



set out "total_bcov_600s.ps"

set ylabel "Branch coverage"

plot '600s_def_all.dat' using (1):3 lw 3, \
'600s_con_all.dat' using (2):3 ls 1 lw 3, \
'600s_imp_all.dat' using (3):3 ls 1 lw 3, \
'600s_ele_all.dat' using (4):3 ls 1 lw 3, \
'600s_det_all.dat' using (5):3 ls 1 lw 3, \
'600s_ori_all.dat' using (6):3 ls 1 lw 3, \
'600s_blo_all.dat' using (7):3 ls 1 lw 3, \
'600s_com_all.dat' using (8):3 ls 1 lw 3



set out "apache_coll_timel.ps"

set ylabel "Instruction coverage"
set yrange [0.0:0.7]
set xrange [0:610]

set xlabel "Time [s]"
set xtics 100
set grid xtics ytics
set key bottom right
set pointsize 1.5

plot '../apache_commons_collection_4.0/600sEnhancementExperiment/RandoopDefault/coverageProfileData.txt' using ($1/1000):2 title 'Default' with linespoints lw 3, \
'../apache_commons_collection_4.0/600sEnhancementExperiment/1.ConstantMiner/coverageProfileData.txt' using ($1/1000):2 title 'Constant mining' with linespoints lw 3, \
'../apache_commons_collection_4.0/600sEnhancementExperiment/4.Impurity/coverageProfileData.txt' using ($1/1000):2 title 'Impurity' with linespoints lw 3, \
'../apache_commons_collection_4.0/600sEnhancementExperiment/3.ElephantBrain/coverageProfileData.txt' using ($1/1000):2 title 'Elephant brain' with linespoints lw 3, \
'../apache_commons_collection_4.0/600sEnhancementExperiment/5.Detective/coverageProfileData.txt' using ($1/1000):2 title 'Detective' with linespoints lw 3, \
'../apache_commons_collection_4.0/600sEnhancementExperiment/2.Orienteering/coverageProfileData.txt' using ($1/1000):2 title 'Orienteering' with linespoints lw 3, \
'../apache_commons_collection_4.0/600sEnhancementExperiment/6.Bloodhound/coverageProfileData.txt' using ($1/1000):2 title 'Bloodhound' with linespoints lw 3, \
'../apache_commons_collection_4.0/600sEnhancementExperiment/CombinedEnhancement/coverageProfileData.txt' using ($1/1000):2 title 'Combined' with linespoints lw 3



set out "apache_coll_timel_b.ps"

set ylabel "Branch coverage"
set yrange [0.0:0.5]

plot '../apache_commons_collection_4.0/600sEnhancementExperiment/RandoopDefault/coverageProfileData.txt' using ($1/1000):3 title 'Default' with linespoints lw 3, \
'../apache_commons_collection_4.0/600sEnhancementExperiment/1.ConstantMiner/coverageProfileData.txt' using ($1/1000):3 title 'Constant mining' with linespoints lw 3, \
'../apache_commons_collection_4.0/600sEnhancementExperiment/4.Impurity/coverageProfileData.txt' using ($1/1000):3 title 'Impurity' with linespoints lw 3, \
'../apache_commons_collection_4.0/600sEnhancementExperiment/3.ElephantBrain/coverageProfileData.txt' using ($1/1000):3 title 'Elephant brain' with linespoints lw 3, \
'../apache_commons_collection_4.0/600sEnhancementExperiment/5.Detective/coverageProfileData.txt' using ($1/1000):3 title 'Detective' with linespoints lw 3, \
'../apache_commons_collection_4.0/600sEnhancementExperiment/2.Orienteering/coverageProfileData.txt' using ($1/1000):3 title 'Orienteering' with linespoints lw 3, \
'../apache_commons_collection_4.0/600sEnhancementExperiment/6.Bloodhound/coverageProfileData.txt' using ($1/1000):3 title 'Bloodhound' with linespoints lw 3, \
'../apache_commons_collection_4.0/600sEnhancementExperiment/CombinedEnhancement/coverageProfileData.txt' using ($1/1000):3 title 'Combined' with linespoints lw 3




set out "apache_comp_timel.ps"

set ylabel "Instruction coverage"
set yrange [0.0:0.6]
set key center right

plot '../apache_commons_compress/600sEnhancementExperiment/RandoopDefault/coverageProfileData.txt' using ($1/1000):2 title 'Default' with linespoints lw 3, \
'../apache_commons_compress/600sEnhancementExperiment/1.ConstantMiner/coverageProfileData.txt' using ($1/1000):2 title 'Constant mining' with linespoints lw 3, \
'../apache_commons_compress/600sEnhancementExperiment/4.Impurity/coverageProfileData.txt' using ($1/1000):2 title 'Impurity' with linespoints lw 3, \
'../apache_commons_compress/600sEnhancementExperiment/3.ElephantBrain/coverageProfileData.txt' using ($1/1000):2 title 'Elephant brain' with linespoints lw 3, \
'../apache_commons_compress/600sEnhancementExperiment/5.Detective/coverageProfileData.txt' using ($1/1000):2 title 'Detective' with linespoints lw 3, \
'../apache_commons_compress/600sEnhancementExperiment/2.Orienteering/coverageProfileData.txt' using ($1/1000):2 title 'Orienteering' with linespoints lw 3, \
'../apache_commons_compress/600sEnhancementExperiment/6.Bloodhound/coverageProfileData.txt' using ($1/1000):2 title 'Bloodhound' with linespoints lw 3, \
'../apache_commons_compress/600sEnhancementExperiment/CombinedEnhancement_seedon=off/coverageProfileData.txt' using ($1/1000):2 title 'Combined' with linespoints lw 3



set out "apache_comp_timel_b.ps"

set ylabel "Branch coverage"
set yrange [0.0:0.35]
set key bottom right


plot '../apache_commons_compress/600sEnhancementExperiment/RandoopDefault/coverageProfileData.txt' using ($1/1000):3 title 'Default' with linespoints lw 3, \
'../apache_commons_compress/600sEnhancementExperiment/1.ConstantMiner/coverageProfileData.txt' using ($1/1000):3 title 'Constant mining' with linespoints lw 3, \
'../apache_commons_compress/600sEnhancementExperiment/4.Impurity/coverageProfileData.txt' using ($1/1000):3 title 'Impurity' with linespoints lw 3, \
'../apache_commons_compress/600sEnhancementExperiment/3.ElephantBrain/coverageProfileData.txt' using ($1/1000):3 title 'Elephant brain' with linespoints lw 3, \
'../apache_commons_compress/600sEnhancementExperiment/5.Detective/coverageProfileData.txt' using ($1/1000):3 title 'Detective' with linespoints lw 3, \
'../apache_commons_compress/600sEnhancementExperiment/2.Orienteering/coverageProfileData.txt' using ($1/1000):3 title 'Orienteering' with linespoints lw 3, \
'../apache_commons_compress/600sEnhancementExperiment/6.Bloodhound/coverageProfileData.txt' using ($1/1000):3 title 'Bloodhound' with linespoints lw 3, \
'../apache_commons_compress/600sEnhancementExperiment/CombinedEnhancement_seedon=off/coverageProfileData.txt' using ($1/1000):3 title 'Combined' with linespoints lw 3



set out "apache_math_timel.ps"

set ylabel "Instruction coverage"
set yrange [0.0:0.6]

plot '../apache_commons_math/600sEnhancementExperiment/RandoopDefault/coverageProfileData.txt' using ($1/1000):2 title 'Default' with linespoints lw 3, \
'../apache_commons_math/600sEnhancementExperiment/1.ConstantMiner/coverageProfileData.txt' using ($1/1000):2 title 'Constant mining' with linespoints lw 3, \
'../apache_commons_math/600sEnhancementExperiment/4.Impurity/coverageProfileData.txt' using ($1/1000):2 title 'Impurity' with linespoints lw 3, \
'../apache_commons_math/600sEnhancementExperiment/3.ElephantBrain/coverageProfileData.txt' using ($1/1000):2 title 'Elephant brain' with linespoints lw 3, \
'../apache_commons_math/600sEnhancementExperiment/5.Detective/coverageProfileData.txt' using ($1/1000):2 title 'Detective' with linespoints lw 3, \
'../apache_commons_math/600sEnhancementExperiment/2.Orienteering/coverageProfileData.txt' using ($1/1000):2 title 'Orienteering' with linespoints lw 3, \
'../apache_commons_math/600sEnhancementExperiment/6.Bloodhound/coverageProfileData.txt' using ($1/1000):2 title 'Bloodhound' with linespoints lw 3, \
'../apache_commons_math/600sEnhancementExperiment/CombinedEnhancement_seedon=off_4G/coverageProfileData.txt' using ($1/1000):2 title 'Combined' with linespoints lw 3



set out "apache_math_timel_b.ps"

set ylabel "Branch coverage"
set yrange [0.0:0.35]

plot '../apache_commons_math/600sEnhancementExperiment/RandoopDefault/coverageProfileData.txt' using ($1/1000):3 title 'Default' with linespoints lw 3, \
'../apache_commons_math/600sEnhancementExperiment/1.ConstantMiner/coverageProfileData.txt' using ($1/1000):3 title 'Constant mining' with linespoints lw 3, \
'../apache_commons_math/600sEnhancementExperiment/4.Impurity/coverageProfileData.txt' using ($1/1000):3 title 'Impurity' with linespoints lw 3, \
'../apache_commons_math/600sEnhancementExperiment/3.ElephantBrain/coverageProfileData.txt' using ($1/1000):3 title 'Elephant brain' with linespoints lw 3, \
'../apache_commons_math/600sEnhancementExperiment/5.Detective/coverageProfileData.txt' using ($1/1000):3 title 'Detective' with linespoints lw 3, \
'../apache_commons_math/600sEnhancementExperiment/2.Orienteering/coverageProfileData.txt' using ($1/1000):3 title 'Orienteering' with linespoints lw 3, \
'../apache_commons_math/600sEnhancementExperiment/6.Bloodhound/coverageProfileData.txt' using ($1/1000):3 title 'Bloodhound' with linespoints lw 3, \
'../apache_commons_math/600sEnhancementExperiment/CombinedEnhancement_seedon=off_4G/coverageProfileData.txt' using ($1/1000):3 title 'Combined' with linespoints lw 3



set out "apache_prim_timel.ps"

set ylabel "Instruction coverage"
set yrange [0.0:0.7]

plot '../apache_primitive/600sEnhancementExperiment/RandoopDefault/coverageProfileData.txt' using ($1/1000):2 title 'Default' with linespoints lw 3, \
'../apache_primitive/600sEnhancementExperiment/1.ConstantMiner/coverageProfileData.txt' using ($1/1000):2 title 'Constant mining' with linespoints lw 3, \
'../apache_primitive/600sEnhancementExperiment/4.Impurity/coverageProfileData.txt' using ($1/1000):2 title 'Impurity' with linespoints lw 3, \
'../apache_primitive/600sEnhancementExperiment/3.ElephantBrain/coverageProfileData.txt' using ($1/1000):2 title 'Elephant brain' with linespoints lw 3, \
'../apache_primitive/600sEnhancementExperiment/5.Detective/coverageProfileData.txt' using ($1/1000):2 title 'Detective' with linespoints lw 3, \
'../apache_primitive/600sEnhancementExperiment/2.Orienteering/coverageProfileData.txt' using ($1/1000):2 title 'Orienteering' with linespoints lw 3, \
'../apache_primitive/600sEnhancementExperiment/6.Bloodhound/coverageProfileData.txt' using ($1/1000):2 title 'Bloodhound' with linespoints lw 3, \
'../apache_primitive/600sEnhancementExperiment/CombinedEnhancement/coverageProfileData.txt' using ($1/1000):2 title 'Combined' with linespoints lw 3



set out "apache_prim_timel_b.ps"

set ylabel "Branch coverage"
set yrange [0.0:0.8]

plot '../apache_primitive/600sEnhancementExperiment/RandoopDefault/coverageProfileData.txt' using ($1/1000):3 title 'Default' with linespoints lw 3, \
'../apache_primitive/600sEnhancementExperiment/1.ConstantMiner/coverageProfileData.txt' using ($1/1000):3 title 'Constant mining' with linespoints lw 3, \
'../apache_primitive/600sEnhancementExperiment/4.Impurity/coverageProfileData.txt' using ($1/1000):3 title 'Impurity' with linespoints lw 3, \
'../apache_primitive/600sEnhancementExperiment/3.ElephantBrain/coverageProfileData.txt' using ($1/1000):3 title 'Elephant brain' with linespoints lw 3, \
'../apache_primitive/600sEnhancementExperiment/5.Detective/coverageProfileData.txt' using ($1/1000):3 title 'Detective' with linespoints lw 3, \
'../apache_primitive/600sEnhancementExperiment/2.Orienteering/coverageProfileData.txt' using ($1/1000):3 title 'Orienteering' with linespoints lw 3, \
'../apache_primitive/600sEnhancementExperiment/6.Bloodhound/coverageProfileData.txt' using ($1/1000):3 title 'Bloodhound' with linespoints lw 3, \
'../apache_primitive/600sEnhancementExperiment/CombinedEnhancement/coverageProfileData.txt' using ($1/1000):3 title 'Combined' with linespoints lw 3



set out "javax_mail_timel.ps"

set ylabel "Instruction coverage"
set yrange [0.0:0.5]

plot '../javax_mail/600sEnhancementExperiment/RandoopDefault/coverageProfileData.txt' using ($1/1000):2 title 'Default' with linespoints lw 3, \
'../javax_mail/600sEnhancementExperiment/1.ConstantMiner/coverageProfileData.txt' using ($1/1000):2 title 'Constant mining' with linespoints lw 3, \
'../javax_mail/600sEnhancementExperiment/4.Impurity/coverageProfileData.txt' using ($1/1000):2 title 'Impurity' with linespoints lw 3, \
'../javax_mail/600sEnhancementExperiment/3.ElephantBrain/coverageProfileData.txt' using ($1/1000):2 title 'Elephant brain' with linespoints lw 3, \
'../javax_mail/600sEnhancementExperiment/5.Detective/coverageProfileData.txt' using ($1/1000):2 title 'Detective' with linespoints lw 3, \
'../javax_mail/600sEnhancementExperiment/2.Orienteering/coverageProfileData.txt' using ($1/1000):2 title 'Orienteering' with linespoints lw 3, \
'../javax_mail/600sEnhancementExperiment/6.Bloodhound/coverageProfileData.txt' using ($1/1000):2 title 'Bloodhound' with linespoints lw 3, \
'../javax_mail/600sEnhancementExperiment/CombinedEnhancement/coverageProfileData.txt' using ($1/1000):2 title 'Combined' with linespoints lw 3



set out "javax_mail_timel_b.ps"

set ylabel "Branch coverage"
set yrange [0.0:0.35]

plot '../javax_mail/600sEnhancementExperiment/RandoopDefault/coverageProfileData.txt' using ($1/1000):3 title 'Default' with linespoints lw 3, \
'../javax_mail/600sEnhancementExperiment/1.ConstantMiner/coverageProfileData.txt' using ($1/1000):3 title 'Constant mining' with linespoints lw 3, \
'../javax_mail/600sEnhancementExperiment/4.Impurity/coverageProfileData.txt' using ($1/1000):3 title 'Impurity' with linespoints lw 3, \
'../javax_mail/600sEnhancementExperiment/3.ElephantBrain/coverageProfileData.txt' using ($1/1000):3 title 'Elephant brain' with linespoints lw 3, \
'../javax_mail/600sEnhancementExperiment/5.Detective/coverageProfileData.txt' using ($1/1000):3 title 'Detective' with linespoints lw 3, \
'../javax_mail/600sEnhancementExperiment/2.Orienteering/coverageProfileData.txt' using ($1/1000):3 title 'Orienteering' with linespoints lw 3, \
'../javax_mail/600sEnhancementExperiment/6.Bloodhound/coverageProfileData.txt' using ($1/1000):3 title 'Bloodhound' with linespoints lw 3, \
'../javax_mail/600sEnhancementExperiment/CombinedEnhancement/coverageProfileData.txt' using ($1/1000):3 title 'Combined' with linespoints lw 3



set out "ASM5.0.1_timel.ps"

set ylabel "Instruction coverage"
set yrange [0.0:0.45]
set key opaque

plot '../ASM5.0.1/600sEnhancementExperiment_4G/RandoopDefault/coverageProfileData.txt' using ($1/1000):2 title 'Default' with linespoints lw 3, \
'../ASM5.0.1/600sEnhancementExperiment_4G/1.ConstantMiner/coverageProfileData.txt' using ($1/1000):2 title 'Constant mining' with linespoints lw 3, \
'../ASM5.0.1/600sEnhancementExperiment_4G/4.Impurity/coverageProfileData.txt' using ($1/1000):2 title 'Impurity' with linespoints lw 3, \
'../ASM5.0.1/600sEnhancementExperiment_4G/3.ElephantBrain/coverageProfileData.txt' using ($1/1000):2 title 'Elephant brain' with linespoints lw 3, \
'../ASM5.0.1/600sEnhancementExperiment_4G/5.Detective/coverageProfileData.txt' using ($1/1000):2 title 'Detective' with linespoints lw 3, \
'../ASM5.0.1/600sEnhancementExperiment_4G/2.Orienteering/coverageProfileData.txt' using ($1/1000):2 title 'Orienteering' with linespoints lw 3, \
'../ASM5.0.1/600sEnhancementExperiment_4G/6.Bloodhound/coverageProfileData.txt' using ($1/1000):2 title 'Bloodhound' with linespoints lw 3, \
'../ASM5.0.1/600sEnhancementExperiment_4G/CombinedEnhancement/coverageProfileData.txt' using ($1/1000):2 title 'Combined' with linespoints lw 3



set out "ASM5.0.1_timel_b.ps"

set ylabel "Branch coverage"
set yrange [0.0:0.3]

plot '../ASM5.0.1/600sEnhancementExperiment_4G/RandoopDefault/coverageProfileData.txt' using ($1/1000):3 title 'Default' with linespoints lw 3, \
'../ASM5.0.1/600sEnhancementExperiment_4G/1.ConstantMiner/coverageProfileData.txt' using ($1/1000):3 title 'Constant mining' with linespoints lw 3, \
'../ASM5.0.1/600sEnhancementExperiment_4G/4.Impurity/coverageProfileData.txt' using ($1/1000):3 title 'Impurity' with linespoints lw 3, \
'../ASM5.0.1/600sEnhancementExperiment_4G/3.ElephantBrain/coverageProfileData.txt' using ($1/1000):3 title 'Elephant brain' with linespoints lw 3, \
'../ASM5.0.1/600sEnhancementExperiment_4G/5.Detective/coverageProfileData.txt' using ($1/1000):3 title 'Detective' with linespoints lw 3, \
'../ASM5.0.1/600sEnhancementExperiment_4G/2.Orienteering/coverageProfileData.txt' using ($1/1000):3 title 'Orienteering' with linespoints lw 3, \
'../ASM5.0.1/600sEnhancementExperiment_4G/6.Bloodhound/coverageProfileData.txt' using ($1/1000):3 title 'Bloodhound' with linespoints lw 3, \
'../ASM5.0.1/600sEnhancementExperiment_4G/CombinedEnhancement/coverageProfileData.txt' using ($1/1000):3 title 'Combined' with linespoints lw 3

set key noopaque

set out "Guava_timel.ps"

set ylabel "Instruction coverage"
set yrange [0.0:0.5]

plot '../Guava/600sEnhancementExperiment_4G/RandoopDefault/coverageProfileData.txt' using ($1/1000):2 title 'Default' with linespoints lw 3, \
'../Guava/600sEnhancementExperiment_4G/1.ConstantMiner/coverageProfileData.txt' using ($1/1000):2 title 'Constant mining' with linespoints lw 3, \
'../Guava/600sEnhancementExperiment_4G/4.Impurity/coverageProfileData.txt' using ($1/1000):2 title 'Impurity' with linespoints lw 3, \
'../Guava/600sEnhancementExperiment_4G/3.ElephantBrain/coverageProfileData.txt' using ($1/1000):2 title 'Elephant brain' with linespoints lw 3, \
'../Guava/600sEnhancementExperiment_4G/5.Detective/coverageProfileData.txt' using ($1/1000):2 title 'Detective' with linespoints lw 3, \
'../Guava/600sEnhancementExperiment_4G/2.Orienteering/coverageProfileData.txt' using ($1/1000):2 title 'Orienteering' with linespoints lw 3, \
'../Guava/600sEnhancementExperiment_4G/6.Bloodhound/coverageProfileData.txt' using ($1/1000):2 title 'Bloodhound' with linespoints lw 3, \
'../Guava/600sEnhancementExperiment_4G/CombinedEnhancement/coverageProfileData.txt' using ($1/1000):2 title 'Combined' with linespoints lw 3



set out "Guava_timel_b.ps"

set ylabel "Branch coverage"
set yrange [0.0:0.4]

plot '../Guava/600sEnhancementExperiment_4G/RandoopDefault/coverageProfileData.txt' using ($1/1000):3 title 'Default' with linespoints lw 3, \
'../Guava/600sEnhancementExperiment_4G/1.ConstantMiner/coverageProfileData.txt' using ($1/1000):3 title 'Constant mining' with linespoints lw 3, \
'../Guava/600sEnhancementExperiment_4G/4.Impurity/coverageProfileData.txt' using ($1/1000):3 title 'Impurity' with linespoints lw 3, \
'../Guava/600sEnhancementExperiment_4G/3.ElephantBrain/coverageProfileData.txt' using ($1/1000):3 title 'Elephant brain' with linespoints lw 3, \
'../Guava/600sEnhancementExperiment_4G/5.Detective/coverageProfileData.txt' using ($1/1000):3 title 'Detective' with linespoints lw 3, \
'../Guava/600sEnhancementExperiment_4G/2.Orienteering/coverageProfileData.txt' using ($1/1000):3 title 'Orienteering' with linespoints lw 3, \
'../Guava/600sEnhancementExperiment_4G/6.Bloodhound/coverageProfileData.txt' using ($1/1000):3 title 'Bloodhound' with linespoints lw 3, \
'../Guava/600sEnhancementExperiment_4G/CombinedEnhancement/coverageProfileData.txt' using ($1/1000):3 title 'Combined' with linespoints lw 3



set out "JSAP_timel.ps"

set ylabel "Instruction coverage"
set yrange [0.0:0.7]

plot '../JSAP/600sEnhancementExperiment/RandoopDefault/coverageProfileData.txt' using ($1/1000):2 title 'Default' with linespoints lw 3, \
'../JSAP/600sEnhancementExperiment/1.ConstantMiner/coverageProfileData.txt' using ($1/1000):2 title 'Constant mining' with linespoints lw 3, \
'../JSAP/600sEnhancementExperiment/4.Impurity/coverageProfileData.txt' using ($1/1000):2 title 'Impurity' with linespoints lw 3, \
'../JSAP/600sEnhancementExperiment/3.ElephantBrain/coverageProfileData.txt' using ($1/1000):2 title 'Elephant brain' with linespoints lw 3, \
'../JSAP/600sEnhancementExperiment/5.Detective/coverageProfileData.txt' using ($1/1000):2 title 'Detective' with linespoints lw 3, \
'../JSAP/600sEnhancementExperiment/2.Orienteering/coverageProfileData.txt' using ($1/1000):2 title 'Orienteering' with linespoints lw 3, \
'../JSAP/600sEnhancementExperiment/6.Bloodhound/coverageProfileData.txt' using ($1/1000):2 title 'Bloodhound' with linespoints lw 3, \
'../JSAP/600sEnhancementExperiment/CombinedEnhancement/coverageProfileData.txt' using ($1/1000):2 title 'Combined' with linespoints lw 3



set out "JSAP_timel_b.ps"

set ylabel "Branch coverage"
set yrange [0.0:0.6]

plot '../JSAP/600sEnhancementExperiment/RandoopDefault/coverageProfileData.txt' using ($1/1000):3 title 'Default' with linespoints lw 3, \
'../JSAP/600sEnhancementExperiment/1.ConstantMiner/coverageProfileData.txt' using ($1/1000):3 title 'Constant mining' with linespoints lw 3, \
'../JSAP/600sEnhancementExperiment/4.Impurity/coverageProfileData.txt' using ($1/1000):3 title 'Impurity' with linespoints lw 3, \
'../JSAP/600sEnhancementExperiment/3.ElephantBrain/coverageProfileData.txt' using ($1/1000):3 title 'Elephant brain' with linespoints lw 3, \
'../JSAP/600sEnhancementExperiment/5.Detective/coverageProfileData.txt' using ($1/1000):3 title 'Detective' with linespoints lw 3, \
'../JSAP/600sEnhancementExperiment/2.Orienteering/coverageProfileData.txt' using ($1/1000):3 title 'Orienteering' with linespoints lw 3, \
'../JSAP/600sEnhancementExperiment/6.Bloodhound/coverageProfileData.txt' using ($1/1000):3 title 'Bloodhound' with linespoints lw 3, \
'../JSAP/600sEnhancementExperiment/CombinedEnhancement/coverageProfileData.txt' using ($1/1000):3 title 'Combined' with linespoints lw 3



set out "Mango_timel.ps"

set ylabel "Instruction coverage"
set yrange [0.0:0.9]

plot '../Mango/600sEnhancementExperiment/RandoopDefault/coverageProfileData.txt' using ($1/1000):2 title 'Default' with linespoints lw 3, \
'../Mango/600sEnhancementExperiment/1.ConstantMiner/coverageProfileData.txt' using ($1/1000):2 title 'Constant mining' with linespoints lw 3, \
'../Mango/600sEnhancementExperiment/4.Impurity/coverageProfileData.txt' using ($1/1000):2 title 'Impurity' with linespoints lw 3, \
'../Mango/600sEnhancementExperiment/3.ElephantBrain/coverageProfileData.txt' using ($1/1000):2 title 'Elephant brain' with linespoints lw 3, \
'../Mango/600sEnhancementExperiment/5.Detective/coverageProfileData.txt' using ($1/1000):2 title 'Detective' with linespoints lw 3, \
'../Mango/600sEnhancementExperiment/2.Orienteering/coverageProfileData.txt' using ($1/1000):2 title 'Orienteering' with linespoints lw 3, \
'../Mango/600sEnhancementExperiment/6.Bloodhound/coverageProfileData.txt' using ($1/1000):2 title 'Bloodhound' with linespoints lw 3, \
'../Mango/600sEnhancementExperiment/CombinedEnhancement/coverageProfileData.txt' using ($1/1000):2 title 'Combined' with linespoints lw 3



set out "Mango_timel_b.ps"

set ylabel "Branch coverage"
set yrange [0.0:0.7]

plot '../Mango/600sEnhancementExperiment/RandoopDefault/coverageProfileData.txt' using ($1/1000):3 title 'Default' with linespoints lw 3, \
'../Mango/600sEnhancementExperiment/1.ConstantMiner/coverageProfileData.txt' using ($1/1000):3 title 'Constant mining' with linespoints lw 3, \
'../Mango/600sEnhancementExperiment/4.Impurity/coverageProfileData.txt' using ($1/1000):3 title 'Impurity' with linespoints lw 3, \
'../Mango/600sEnhancementExperiment/3.ElephantBrain/coverageProfileData.txt' using ($1/1000):3 title 'Elephant brain' with linespoints lw 3, \
'../Mango/600sEnhancementExperiment/5.Detective/coverageProfileData.txt' using ($1/1000):3 title 'Detective' with linespoints lw 3, \
'../Mango/600sEnhancementExperiment/2.Orienteering/coverageProfileData.txt' using ($1/1000):3 title 'Orienteering' with linespoints lw 3, \
'../Mango/600sEnhancementExperiment/6.Bloodhound/coverageProfileData.txt' using ($1/1000):3 title 'Bloodhound' with linespoints lw 3, \
'../Mango/600sEnhancementExperiment/CombinedEnhancement/coverageProfileData.txt' using ($1/1000):3 title 'Combined' with linespoints lw 3



set out "Rhino_timel.ps"

set ylabel "Instruction coverage"
set yrange [0.0:0.25]

plot '../Rhino/600sEnhancementExperiment/RandoopDefault/coverageProfileData.txt' using ($1/1000):2 title 'Default' with linespoints lw 3, \
'../Rhino/600sEnhancementExperiment/1.ConstantMiner/coverageProfileData.txt' using ($1/1000):2 title 'Constant mining' with linespoints lw 3, \
'../Rhino/600sEnhancementExperiment/4.Impurity/coverageProfileData.txt' using ($1/1000):2 title 'Impurity' with linespoints lw 3, \
'../Rhino/600sEnhancementExperiment/3.ElephantBrain/coverageProfileData.txt' using ($1/1000):2 title 'Elephant brain' with linespoints lw 3, \
'../Rhino/600sEnhancementExperiment/5.Detective/coverageProfileData.txt' using ($1/1000):2 title 'Detective' with linespoints lw 3, \
'../Rhino/600sEnhancementExperiment/2.Orienteering/coverageProfileData.txt' using ($1/1000):2 title 'Orienteering' with linespoints lw 3, \
'../Rhino/600sEnhancementExperiment/6.Bloodhound/coverageProfileData.txt' using ($1/1000):2 title 'Bloodhound' with linespoints lw 3, \
'../Rhino/600sEnhancementExperiment/CombinedEnhancement/coverageProfileData.txt' using ($1/1000):2 title 'Combined' with linespoints lw 3



set out "Rhino_timel_b.ps"

set ylabel "Branch coverage"
set yrange [0.0:0.2]

plot '../Rhino/600sEnhancementExperiment/RandoopDefault/coverageProfileData.txt' using ($1/1000):3 title 'Default' with linespoints lw 3, \
'../Rhino/600sEnhancementExperiment/1.ConstantMiner/coverageProfileData.txt' using ($1/1000):3 title 'Constant mining' with linespoints lw 3, \
'../Rhino/600sEnhancementExperiment/4.Impurity/coverageProfileData.txt' using ($1/1000):3 title 'Impurity' with linespoints lw 3, \
'../Rhino/600sEnhancementExperiment/3.ElephantBrain/coverageProfileData.txt' using ($1/1000):3 title 'Elephant brain' with linespoints lw 3, \
'../Rhino/600sEnhancementExperiment/5.Detective/coverageProfileData.txt' using ($1/1000):3 title 'Detective' with linespoints lw 3, \
'../Rhino/600sEnhancementExperiment/2.Orienteering/coverageProfileData.txt' using ($1/1000):3 title 'Orienteering' with linespoints lw 3, \
'../Rhino/600sEnhancementExperiment/6.Bloodhound/coverageProfileData.txt' using ($1/1000):3 title 'Bloodhound' with linespoints lw 3, \
'../Rhino/600sEnhancementExperiment/CombinedEnhancement/coverageProfileData.txt' using ($1/1000):3 title 'Combined' with linespoints lw 3



set out "SAT4J_core_timel.ps"

set ylabel "Instruction coverage"
set yrange [0.0:0.7]

plot '../SAT4J_core/600sEnhancementExperiment/RandoopDefault/coverageProfileData.txt' using ($1/1000):2 title 'Default' with linespoints lw 3, \
'../SAT4J_core/600sEnhancementExperiment/1.ConstantMiner/coverageProfileData.txt' using ($1/1000):2 title 'Constant mining' with linespoints lw 3, \
'../SAT4J_core/600sEnhancementExperiment/4.Impurity/coverageProfileData.txt' using ($1/1000):2 title 'Impurity' with linespoints lw 3, \
'../SAT4J_core/600sEnhancementExperiment/3.ElephantBrain/coverageProfileData.txt' using ($1/1000):2 title 'Elephant brain' with linespoints lw 3, \
'../SAT4J_core/600sEnhancementExperiment/5.Detective/coverageProfileData.txt' using ($1/1000):2 title 'Detective' with linespoints lw 3, \
'../SAT4J_core/600sEnhancementExperiment/2.Orienteering/coverageProfileData.txt' using ($1/1000):2 title 'Orienteering' with linespoints lw 3, \
'../SAT4J_core/600sEnhancementExperiment/6.Bloodhound/coverageProfileData.txt' using ($1/1000):2 title 'Bloodhound' with linespoints lw 3, \
'../SAT4J_core/600sEnhancementExperiment/CombinedEnhancement/coverageProfileData.txt' using ($1/1000):2 title 'Combined' with linespoints lw 3



set out "SAT4J_core_timel_b.ps"

set ylabel "Branch coverage"
set yrange [0.0:0.5]

plot '../SAT4J_core/600sEnhancementExperiment/RandoopDefault/coverageProfileData.txt' using ($1/1000):3 title 'Default' with linespoints lw 3, \
'../SAT4J_core/600sEnhancementExperiment/1.ConstantMiner/coverageProfileData.txt' using ($1/1000):3 title 'Constant mining' with linespoints lw 3, \
'../SAT4J_core/600sEnhancementExperiment/4.Impurity/coverageProfileData.txt' using ($1/1000):3 title 'Impurity' with linespoints lw 3, \
'../SAT4J_core/600sEnhancementExperiment/3.ElephantBrain/coverageProfileData.txt' using ($1/1000):3 title 'Elephant brain' with linespoints lw 3, \
'../SAT4J_core/600sEnhancementExperiment/5.Detective/coverageProfileData.txt' using ($1/1000):3 title 'Detective' with linespoints lw 3, \
'../SAT4J_core/600sEnhancementExperiment/2.Orienteering/coverageProfileData.txt' using ($1/1000):3 title 'Orienteering' with linespoints lw 3, \
'../SAT4J_core/600sEnhancementExperiment/6.Bloodhound/coverageProfileData.txt' using ($1/1000):3 title 'Bloodhound' with linespoints lw 3, \
'../SAT4J_core/600sEnhancementExperiment/CombinedEnhancement/coverageProfileData.txt' using ($1/1000):3 title 'Combined' with linespoints lw 3



set out "TinySql_timel.ps"

set ylabel "Instruction coverage"
set yrange [0.0:0.35]

plot '../TinySql/600sEnhancementExperiment/RandoopDefault/coverageProfileData.txt' using ($1/1000):2 title 'Default' with linespoints lw 3, \
'../TinySql/600sEnhancementExperiment/1.ConstantMiner/coverageProfileData.txt' using ($1/1000):2 title 'Constant mining' with linespoints lw 3, \
'../TinySql/600sEnhancementExperiment/4.Impurity/coverageProfileData.txt' using ($1/1000):2 title 'Impurity' with linespoints lw 3, \
'../TinySql/600sEnhancementExperiment/3.ElephantBrain/coverageProfileData.txt' using ($1/1000):2 title 'Elephant brain' with linespoints lw 3, \
'../TinySql/600sEnhancementExperiment/5.Detective/coverageProfileData.txt' using ($1/1000):2 title 'Detective' with linespoints lw 3, \
'../TinySql/600sEnhancementExperiment/2.Orienteering/coverageProfileData.txt' using ($1/1000):2 title 'Orienteering' with linespoints lw 3, \
'../TinySql/600sEnhancementExperiment/6.Bloodhound/coverageProfileData.txt' using ($1/1000):2 title 'Bloodhound' with linespoints lw 3, \
'../TinySql/600sEnhancementExperiment/CombinedEnhancement/coverageProfileData.txt' using ($1/1000):2 title 'Combined' with linespoints lw 3



set out "TinySql_timel_b.ps"

set ylabel "Branch coverage"
set yrange [0.0:0.25]

plot '../TinySql/600sEnhancementExperiment/RandoopDefault/coverageProfileData.txt' using ($1/1000):3 title 'Default' with linespoints lw 3, \
'../TinySql/600sEnhancementExperiment/1.ConstantMiner/coverageProfileData.txt' using ($1/1000):3 title 'Constant mining' with linespoints lw 3, \
'../TinySql/600sEnhancementExperiment/4.Impurity/coverageProfileData.txt' using ($1/1000):3 title 'Impurity' with linespoints lw 3, \
'../TinySql/600sEnhancementExperiment/3.ElephantBrain/coverageProfileData.txt' using ($1/1000):3 title 'Elephant brain' with linespoints lw 3, \
'../TinySql/600sEnhancementExperiment/5.Detective/coverageProfileData.txt' using ($1/1000):3 title 'Detective' with linespoints lw 3, \
'../TinySql/600sEnhancementExperiment/2.Orienteering/coverageProfileData.txt' using ($1/1000):3 title 'Orienteering' with linespoints lw 3, \
'../TinySql/600sEnhancementExperiment/6.Bloodhound/coverageProfileData.txt' using ($1/1000):3 title 'Bloodhound' with linespoints lw 3, \
'../TinySql/600sEnhancementExperiment/CombinedEnhancement/coverageProfileData.txt' using ($1/1000):3 title 'Combined' with linespoints lw 3



set out "apache_cli_timel.ps"

set ylabel "Instruction coverage"
set yrange [0.0:0.8]

plot '../apache_cli/600sEnhancementExperiment/RandoopDefault/coverageProfileData.txt' using ($1/1000):2 title 'Default' with linespoints lw 3, \
'../apache_cli/600sEnhancementExperiment/1.ConstantMiner/coverageProfileData.txt' using ($1/1000):2 title 'Constant mining' with linespoints lw 3, \
'../apache_cli/600sEnhancementExperiment/4.Impurity/coverageProfileData.txt' using ($1/1000):2 title 'Impurity' with linespoints lw 3, \
'../apache_cli/600sEnhancementExperiment/3.ElephantBrain/coverageProfileData.txt' using ($1/1000):2 title 'Elephant brain' with linespoints lw 3, \
'../apache_cli/600sEnhancementExperiment/5.Detective/coverageProfileData.txt' using ($1/1000):2 title 'Detective' with linespoints lw 3, \
'../apache_cli/600sEnhancementExperiment/2.Orienteering/coverageProfileData.txt' using ($1/1000):2 title 'Orienteering' with linespoints lw 3, \
'../apache_cli/600sEnhancementExperiment/6.Bloodhound/coverageProfileData.txt' using ($1/1000):2 title 'Bloodhound' with linespoints lw 3, \
'../apache_cli/600sEnhancementExperiment/CombinedEnhancement/coverageProfileData.txt' using ($1/1000):2 title 'Combined' with linespoints lw 3



set out "apache_cli_timel_b.ps"

set ylabel "Branch coverage"
set yrange [0.0:0.7]

plot '../apache_cli/600sEnhancementExperiment/RandoopDefault/coverageProfileData.txt' using ($1/1000):3 title 'Default' with linespoints lw 3, \
'../apache_cli/600sEnhancementExperiment/1.ConstantMiner/coverageProfileData.txt' using ($1/1000):3 title 'Constant mining' with linespoints lw 3, \
'../apache_cli/600sEnhancementExperiment/4.Impurity/coverageProfileData.txt' using ($1/1000):3 title 'Impurity' with linespoints lw 3, \
'../apache_cli/600sEnhancementExperiment/3.ElephantBrain/coverageProfileData.txt' using ($1/1000):3 title 'Elephant brain' with linespoints lw 3, \
'../apache_cli/600sEnhancementExperiment/5.Detective/coverageProfileData.txt' using ($1/1000):3 title 'Detective' with linespoints lw 3, \
'../apache_cli/600sEnhancementExperiment/2.Orienteering/coverageProfileData.txt' using ($1/1000):3 title 'Orienteering' with linespoints lw 3, \
'../apache_cli/600sEnhancementExperiment/6.Bloodhound/coverageProfileData.txt' using ($1/1000):3 title 'Bloodhound' with linespoints lw 3, \
'../apache_cli/600sEnhancementExperiment/CombinedEnhancement/coverageProfileData.txt' using ($1/1000):3 title 'Combined' with linespoints lw 3



set out "apache_codec_timel.ps"

set ylabel "Instruction coverage"
set yrange [0.0:1.0]

plot '../apache_codec/600sEnhancementExperiment/RandoopDefault/coverageProfileData.txt' using ($1/1000):2 title 'Default' with linespoints lw 3, \
'../apache_codec/600sEnhancementExperiment/1.ConstantMiner/coverageProfileData.txt' using ($1/1000):2 title 'Constant mining' with linespoints lw 3, \
'../apache_codec/600sEnhancementExperiment/4.Impurity/coverageProfileData.txt' using ($1/1000):2 title 'Impurity' with linespoints lw 3, \
'../apache_codec/600sEnhancementExperiment/3.ElephantBrain/coverageProfileData.txt' using ($1/1000):2 title 'Elephant brain' with linespoints lw 3, \
'../apache_codec/600sEnhancementExperiment/5.Detective/coverageProfileData.txt' using ($1/1000):2 title 'Detective' with linespoints lw 3, \
'../apache_codec/600sEnhancementExperiment/2.Orienteering/coverageProfileData.txt' using ($1/1000):2 title 'Orienteering' with linespoints lw 3, \
'../apache_codec/600sEnhancementExperiment/6.Bloodhound/coverageProfileData.txt' using ($1/1000):2 title 'Bloodhound' with linespoints lw 3, \
'../apache_codec/600sEnhancementExperiment/CombinedEnhancement/coverageProfileData.txt' using ($1/1000):2 title 'Combined' with linespoints lw 3



set out "apache_codec_timel_b.ps"

set ylabel "Branch coverage"
set yrange [0.0:0.8]

plot '../apache_codec/600sEnhancementExperiment/RandoopDefault/coverageProfileData.txt' using ($1/1000):3 title 'Default' with linespoints lw 3, \
'../apache_codec/600sEnhancementExperiment/1.ConstantMiner/coverageProfileData.txt' using ($1/1000):3 title 'Constant mining' with linespoints lw 3, \
'../apache_codec/600sEnhancementExperiment/4.Impurity/coverageProfileData.txt' using ($1/1000):3 title 'Impurity' with linespoints lw 3, \
'../apache_codec/600sEnhancementExperiment/3.ElephantBrain/coverageProfileData.txt' using ($1/1000):3 title 'Elephant brain' with linespoints lw 3, \
'../apache_codec/600sEnhancementExperiment/5.Detective/coverageProfileData.txt' using ($1/1000):3 title 'Detective' with linespoints lw 3, \
'../apache_codec/600sEnhancementExperiment/2.Orienteering/coverageProfileData.txt' using ($1/1000):3 title 'Orienteering' with linespoints lw 3, \
'../apache_codec/600sEnhancementExperiment/6.Bloodhound/coverageProfileData.txt' using ($1/1000):3 title 'Bloodhound' with linespoints lw 3, \
'../apache_codec/600sEnhancementExperiment/CombinedEnhancement/coverageProfileData.txt' using ($1/1000):3 title 'Combined' with linespoints lw 3



set out "bcel_timel.ps"

set ylabel "Instruction coverage"
set yrange [0.0:0.5]

plot '../bcel/600sEnhancementExperiment/RandoopDefault/coverageProfileData.txt' using ($1/1000):2 title 'Default' with linespoints lw 3, \
'../bcel/600sEnhancementExperiment/1.ConstantMiner/coverageProfileData.txt' using ($1/1000):2 title 'Constant mining' with linespoints lw 3, \
'../bcel/600sEnhancementExperiment/4.Impurity/coverageProfileData.txt' using ($1/1000):2 title 'Impurity' with linespoints lw 3, \
'../bcel/600sEnhancementExperiment/3.ElephantBrain/coverageProfileData.txt' using ($1/1000):2 title 'Elephant brain' with linespoints lw 3, \
'../bcel/600sEnhancementExperiment/5.Detective/coverageProfileData.txt' using ($1/1000):2 title 'Detective' with linespoints lw 3, \
'../bcel/600sEnhancementExperiment/2.Orienteering/coverageProfileData.txt' using ($1/1000):2 title 'Orienteering' with linespoints lw 3, \
'../bcel/600sEnhancementExperiment/6.Bloodhound/coverageProfileData.txt' using ($1/1000):2 title 'Bloodhound' with linespoints lw 3, \
'../bcel/600sEnhancementExperiment/CombinedEnhancement/coverageProfileData.txt' using ($1/1000):2 title 'Combined' with linespoints lw 3



set out "bcel_timel_b.ps"

set ylabel "Branch coverage"
set yrange [0.0:0.4]

plot '../bcel/600sEnhancementExperiment/RandoopDefault/coverageProfileData.txt' using ($1/1000):3 title 'Default' with linespoints lw 3, \
'../bcel/600sEnhancementExperiment/1.ConstantMiner/coverageProfileData.txt' using ($1/1000):3 title 'Constant mining' with linespoints lw 3, \
'../bcel/600sEnhancementExperiment/4.Impurity/coverageProfileData.txt' using ($1/1000):3 title 'Impurity' with linespoints lw 3, \
'../bcel/600sEnhancementExperiment/3.ElephantBrain/coverageProfileData.txt' using ($1/1000):3 title 'Elephant brain' with linespoints lw 3, \
'../bcel/600sEnhancementExperiment/5.Detective/coverageProfileData.txt' using ($1/1000):3 title 'Detective' with linespoints lw 3, \
'../bcel/600sEnhancementExperiment/2.Orienteering/coverageProfileData.txt' using ($1/1000):3 title 'Orienteering' with linespoints lw 3, \
'../bcel/600sEnhancementExperiment/6.Bloodhound/coverageProfileData.txt' using ($1/1000):3 title 'Bloodhound' with linespoints lw 3, \
'../bcel/600sEnhancementExperiment/CombinedEnhancement/coverageProfileData.txt' using ($1/1000):3 title 'Combined' with linespoints lw 3



set out "jdom1.0_timel.ps"

set ylabel "Instruction coverage"
set yrange [0.0:0.6]

plot '../jdom1.0/600sEnhancementExperiment/RandoopDefault/coverageProfileData.txt' using ($1/1000):2 title 'Default' with linespoints lw 3, \
'../jdom1.0/600sEnhancementExperiment/1.ConstantMiner/coverageProfileData.txt' using ($1/1000):2 title 'Constant mining' with linespoints lw 3, \
'../jdom1.0/600sEnhancementExperiment/4.Impurity/coverageProfileData.txt' using ($1/1000):2 title 'Impurity' with linespoints lw 3, \
'../jdom1.0/600sEnhancementExperiment/3.ElephantBrain/coverageProfileData.txt' using ($1/1000):2 title 'Elephant brain' with linespoints lw 3, \
'../jdom1.0/600sEnhancementExperiment/5.Detective/coverageProfileData.txt' using ($1/1000):2 title 'Detective' with linespoints lw 3, \
'../jdom1.0/600sEnhancementExperiment/2.Orienteering/coverageProfileData.txt' using ($1/1000):2 title 'Orienteering' with linespoints lw 3, \
'../jdom1.0/600sEnhancementExperiment/6.Bloodhound/coverageProfileData.txt' using ($1/1000):2 title 'Bloodhound' with linespoints lw 3, \
'../jdom1.0/600sEnhancementExperiment/CombinedEnhancement/coverageProfileData.txt' using ($1/1000):2 title 'Combined' with linespoints lw 3



set out "jdom1.0_timel_b.ps"

set ylabel "Branch coverage"
set yrange [0.0:0.4]

plot '../jdom1.0/600sEnhancementExperiment/RandoopDefault/coverageProfileData.txt' using ($1/1000):3 title 'Default' with linespoints lw 3, \
'../jdom1.0/600sEnhancementExperiment/1.ConstantMiner/coverageProfileData.txt' using ($1/1000):3 title 'Constant mining' with linespoints lw 3, \
'../jdom1.0/600sEnhancementExperiment/4.Impurity/coverageProfileData.txt' using ($1/1000):3 title 'Impurity' with linespoints lw 3, \
'../jdom1.0/600sEnhancementExperiment/3.ElephantBrain/coverageProfileData.txt' using ($1/1000):3 title 'Elephant brain' with linespoints lw 3, \
'../jdom1.0/600sEnhancementExperiment/5.Detective/coverageProfileData.txt' using ($1/1000):3 title 'Detective' with linespoints lw 3, \
'../jdom1.0/600sEnhancementExperiment/2.Orienteering/coverageProfileData.txt' using ($1/1000):3 title 'Orienteering' with linespoints lw 3, \
'../jdom1.0/600sEnhancementExperiment/6.Bloodhound/coverageProfileData.txt' using ($1/1000):3 title 'Bloodhound' with linespoints lw 3, \
'../jdom1.0/600sEnhancementExperiment/CombinedEnhancement/coverageProfileData.txt' using ($1/1000):3 title 'Combined' with linespoints lw 3



set out "joda_time_timel.ps"

set ylabel "Instruction coverage"
set yrange [0.0:0.8]

plot '../joda_time/600sEnhancementExperiment/RandoopDefault/coverageProfileData.txt' using ($1/1000):2 title 'Default' with linespoints lw 3, \
'../joda_time/600sEnhancementExperiment/1.ConstantMiner/coverageProfileData.txt' using ($1/1000):2 title 'Constant mining' with linespoints lw 3, \
'../joda_time/600sEnhancementExperiment/4.Impurity/coverageProfileData.txt' using ($1/1000):2 title 'Impurity' with linespoints lw 3, \
'../joda_time/600sEnhancementExperiment/3.ElephantBrain/coverageProfileData.txt' using ($1/1000):2 title 'Elephant brain' with linespoints lw 3, \
'../joda_time/600sEnhancementExperiment/5.Detective/coverageProfileData.txt' using ($1/1000):2 title 'Detective' with linespoints lw 3, \
'../joda_time/600sEnhancementExperiment/2.Orienteering/coverageProfileData.txt' using ($1/1000):2 title 'Orienteering' with linespoints lw 3, \
'../joda_time/600sEnhancementExperiment/6.Bloodhound/coverageProfileData.txt' using ($1/1000):2 title 'Bloodhound' with linespoints lw 3, \
'../joda_time/600sEnhancementExperiment/CombinedEnhancement/coverageProfileData.txt' using ($1/1000):2 title 'Combined' with linespoints lw 3



set out "joda_time_timel_b.ps"

set ylabel "Branch coverage"
set yrange [0.0:0.6]

plot '../joda_time/600sEnhancementExperiment/RandoopDefault/coverageProfileData.txt' using ($1/1000):3 title 'Default' with linespoints lw 3, \
'../joda_time/600sEnhancementExperiment/1.ConstantMiner/coverageProfileData.txt' using ($1/1000):3 title 'Constant mining' with linespoints lw 3, \
'../joda_time/600sEnhancementExperiment/4.Impurity/coverageProfileData.txt' using ($1/1000):3 title 'Impurity' with linespoints lw 3, \
'../joda_time/600sEnhancementExperiment/3.ElephantBrain/coverageProfileData.txt' using ($1/1000):3 title 'Elephant brain' with linespoints lw 3, \
'../joda_time/600sEnhancementExperiment/5.Detective/coverageProfileData.txt' using ($1/1000):3 title 'Detective' with linespoints lw 3, \
'../joda_time/600sEnhancementExperiment/2.Orienteering/coverageProfileData.txt' using ($1/1000):3 title 'Orienteering' with linespoints lw 3, \
'../joda_time/600sEnhancementExperiment/6.Bloodhound/coverageProfileData.txt' using ($1/1000):3 title 'Bloodhound' with linespoints lw 3, \
'../joda_time/600sEnhancementExperiment/CombinedEnhancement/coverageProfileData.txt' using ($1/1000):3 title 'Combined' with linespoints lw 3



set out "15_beanbin_timel.ps"

set ylabel "Instruction coverage"
set yrange [0.0:0.35]

plot '../SF100/15_beanbin/600sEnhancementExperiment/RandoopDefault/coverageProfileData.txt' using ($1/1000):2 title 'Default' with linespoints lw 3, \
'../SF100/15_beanbin/600sEnhancementExperiment/1.ConstantMiner/coverageProfileData.txt' using ($1/1000):2 title 'Constant mining' with linespoints lw 3, \
'../SF100/15_beanbin/600sEnhancementExperiment/4.Impurity/coverageProfileData.txt' using ($1/1000):2 title 'Impurity' with linespoints lw 3, \
'../SF100/15_beanbin/600sEnhancementExperiment/3.ElephantBrain/coverageProfileData.txt' using ($1/1000):2 title 'Elephant brain' with linespoints lw 3, \
'../SF100/15_beanbin/600sEnhancementExperiment/5.Detective/coverageProfileData.txt' using ($1/1000):2 title 'Detective' with linespoints lw 3, \
'../SF100/15_beanbin/600sEnhancementExperiment/2.Orienteering/coverageProfileData.txt' using ($1/1000):2 title 'Orienteering' with linespoints lw 3, \
'../SF100/15_beanbin/600sEnhancementExperiment/6.Bloodhound/coverageProfileData.txt' using ($1/1000):2 title 'Bloodhound' with linespoints lw 3, \
'../SF100/15_beanbin/600sEnhancementExperiment/CombinedEnhancement/coverageProfileData.txt' using ($1/1000):2 title 'Combined' with linespoints lw 3



set out "15_beanbin_timel_b.ps"

set ylabel "Branch coverage"
set yrange [0.0:0.3]

plot '../SF100/15_beanbin/600sEnhancementExperiment/RandoopDefault/coverageProfileData.txt' using ($1/1000):3 title 'Default' with linespoints lw 3, \
'../SF100/15_beanbin/600sEnhancementExperiment/1.ConstantMiner/coverageProfileData.txt' using ($1/1000):3 title 'Constant mining' with linespoints lw 3, \
'../SF100/15_beanbin/600sEnhancementExperiment/4.Impurity/coverageProfileData.txt' using ($1/1000):3 title 'Impurity' with linespoints lw 3, \
'../SF100/15_beanbin/600sEnhancementExperiment/3.ElephantBrain/coverageProfileData.txt' using ($1/1000):3 title 'Elephant brain' with linespoints lw 3, \
'../SF100/15_beanbin/600sEnhancementExperiment/5.Detective/coverageProfileData.txt' using ($1/1000):3 title 'Detective' with linespoints lw 3, \
'../SF100/15_beanbin/600sEnhancementExperiment/2.Orienteering/coverageProfileData.txt' using ($1/1000):3 title 'Orienteering' with linespoints lw 3, \
'../SF100/15_beanbin/600sEnhancementExperiment/6.Bloodhound/coverageProfileData.txt' using ($1/1000):3 title 'Bloodhound' with linespoints lw 3, \
'../SF100/15_beanbin/600sEnhancementExperiment/CombinedEnhancement/coverageProfileData.txt' using ($1/1000):3 title 'Combined' with linespoints lw 3



set out "18_jsecurity_timel.ps"

set ylabel "Instruction coverage"
set yrange [0.0:0.5]

plot '../SF100/18_jsecurity/600sEnhancementExperiment/RandoopDefault/coverageProfileData.txt' using ($1/1000):2 title 'Default' with linespoints lw 3, \
'../SF100/18_jsecurity/600sEnhancementExperiment/1.ConstantMiner/coverageProfileData.txt' using ($1/1000):2 title 'Constant mining' with linespoints lw 3, \
'../SF100/18_jsecurity/600sEnhancementExperiment/4.Impurity/coverageProfileData.txt' using ($1/1000):2 title 'Impurity' with linespoints lw 3, \
'../SF100/18_jsecurity/600sEnhancementExperiment/3.ElephantBrain/coverageProfileData.txt' using ($1/1000):2 title 'Elephant brain' with linespoints lw 3, \
'../SF100/18_jsecurity/600sEnhancementExperiment/5.Detective/coverageProfileData.txt' using ($1/1000):2 title 'Detective' with linespoints lw 3, \
'../SF100/18_jsecurity/600sEnhancementExperiment/2.Orienteering/coverageProfileData.txt' using ($1/1000):2 title 'Orienteering' with linespoints lw 3, \
'../SF100/18_jsecurity/600sEnhancementExperiment/6.Bloodhound/coverageProfileData.txt' using ($1/1000):2 title 'Bloodhound' with linespoints lw 3, \
'../SF100/18_jsecurity/600sEnhancementExperiment/CombinedEnhancement/coverageProfileData.txt' using ($1/1000):2 title 'Combined' with linespoints lw 3



set out "18_jsecurity_timel_b.ps"

set ylabel "Branch coverage"
set yrange [0.0:0.25]

plot '../SF100/18_jsecurity/600sEnhancementExperiment/RandoopDefault/coverageProfileData.txt' using ($1/1000):3 title 'Default' with linespoints lw 3, \
'../SF100/18_jsecurity/600sEnhancementExperiment/1.ConstantMiner/coverageProfileData.txt' using ($1/1000):3 title 'Constant mining' with linespoints lw 3, \
'../SF100/18_jsecurity/600sEnhancementExperiment/4.Impurity/coverageProfileData.txt' using ($1/1000):3 title 'Impurity' with linespoints lw 3, \
'../SF100/18_jsecurity/600sEnhancementExperiment/3.ElephantBrain/coverageProfileData.txt' using ($1/1000):3 title 'Elephant brain' with linespoints lw 3, \
'../SF100/18_jsecurity/600sEnhancementExperiment/5.Detective/coverageProfileData.txt' using ($1/1000):3 title 'Detective' with linespoints lw 3, \
'../SF100/18_jsecurity/600sEnhancementExperiment/2.Orienteering/coverageProfileData.txt' using ($1/1000):3 title 'Orienteering' with linespoints lw 3, \
'../SF100/18_jsecurity/600sEnhancementExperiment/6.Bloodhound/coverageProfileData.txt' using ($1/1000):3 title 'Bloodhound' with linespoints lw 3, \
'../SF100/18_jsecurity/600sEnhancementExperiment/CombinedEnhancement/coverageProfileData.txt' using ($1/1000):3 title 'Combined' with linespoints lw 3



set out "1_tullibee_timel.ps"

set ylabel "Instruction coverage"
set yrange [0.0:0.35]

plot '../SF100/1_tullibee/600sEnhancementExperiment/RandoopDefault/coverageProfileData.txt' using ($1/1000):2 title 'Default' with linespoints lw 3, \
'../SF100/1_tullibee/600sEnhancementExperiment/1.ConstantMiner/coverageProfileData.txt' using ($1/1000):2 title 'Constant mining' with linespoints lw 3, \
'../SF100/1_tullibee/600sEnhancementExperiment/4.Impurity/coverageProfileData.txt' using ($1/1000):2 title 'Impurity' with linespoints lw 3, \
'../SF100/1_tullibee/600sEnhancementExperiment/3.ElephantBrain/coverageProfileData.txt' using ($1/1000):2 title 'Elephant brain' with linespoints lw 3, \
'../SF100/1_tullibee/600sEnhancementExperiment/5.Detective/coverageProfileData.txt' using ($1/1000):2 title 'Detective' with linespoints lw 3, \
'../SF100/1_tullibee/600sEnhancementExperiment/2.Orienteering/coverageProfileData.txt' using ($1/1000):2 title 'Orienteering' with linespoints lw 3, \
'../SF100/1_tullibee/600sEnhancementExperiment/6.Bloodhound/coverageProfileData.txt' using ($1/1000):2 title 'Bloodhound' with linespoints lw 3, \
'../SF100/1_tullibee/600sEnhancementExperiment/CombinedEnhancement/coverageProfileData.txt' using ($1/1000):2 title 'Combined' with linespoints lw 3



set out "1_tullibee_timel_b.ps"

set ylabel "Branch coverage"
set yrange [0.0:0.15]

plot '../SF100/1_tullibee/600sEnhancementExperiment/RandoopDefault/coverageProfileData.txt' using ($1/1000):3 title 'Default' with linespoints lw 3, \
'../SF100/1_tullibee/600sEnhancementExperiment/1.ConstantMiner/coverageProfileData.txt' using ($1/1000):3 title 'Constant mining' with linespoints lw 3, \
'../SF100/1_tullibee/600sEnhancementExperiment/4.Impurity/coverageProfileData.txt' using ($1/1000):3 title 'Impurity' with linespoints lw 3, \
'../SF100/1_tullibee/600sEnhancementExperiment/3.ElephantBrain/coverageProfileData.txt' using ($1/1000):3 title 'Elephant brain' with linespoints lw 3, \
'../SF100/1_tullibee/600sEnhancementExperiment/5.Detective/coverageProfileData.txt' using ($1/1000):3 title 'Detective' with linespoints lw 3, \
'../SF100/1_tullibee/600sEnhancementExperiment/2.Orienteering/coverageProfileData.txt' using ($1/1000):3 title 'Orienteering' with linespoints lw 3, \
'../SF100/1_tullibee/600sEnhancementExperiment/6.Bloodhound/coverageProfileData.txt' using ($1/1000):3 title 'Bloodhound' with linespoints lw 3, \
'../SF100/1_tullibee/600sEnhancementExperiment/CombinedEnhancement/coverageProfileData.txt' using ($1/1000):3 title 'Combined' with linespoints lw 3



set out "20_nekomud_timel.ps"

set ylabel "Instruction coverage"
set yrange [0.0:0.12]

plot '../SF100/20_nekomud/600sEnhancementExperiment/RandoopDefault/coverageProfileData.txt' using ($1/1000):2 title 'Default' with linespoints lw 3, \
'../SF100/20_nekomud/600sEnhancementExperiment/1.ConstantMiner/coverageProfileData.txt' using ($1/1000):2 title 'Constant mining' with linespoints lw 3, \
'../SF100/20_nekomud/600sEnhancementExperiment/4.Impurity/coverageProfileData.txt' using ($1/1000):2 title 'Impurity' with linespoints lw 3, \
'../SF100/20_nekomud/600sEnhancementExperiment/3.ElephantBrain/coverageProfileData.txt' using ($1/1000):2 title 'Elephant brain' with linespoints lw 3, \
'../SF100/20_nekomud/600sEnhancementExperiment/5.Detective/coverageProfileData.txt' using ($1/1000):2 title 'Detective' with linespoints lw 3, \
'../SF100/20_nekomud/600sEnhancementExperiment/2.Orienteering/coverageProfileData.txt' using ($1/1000):2 title 'Orienteering' with linespoints lw 3, \
'../SF100/20_nekomud/600sEnhancementExperiment/6.Bloodhound/coverageProfileData.txt' using ($1/1000):2 title 'Bloodhound' with linespoints lw 3, \
'../SF100/20_nekomud/600sEnhancementExperiment/CombinedEnhancement/coverageProfileData.txt' using ($1/1000):2 title 'Combined' with linespoints lw 3



set out "20_nekomud_timel_b.ps"

set ylabel "Branch coverage"
set yrange [0.0:0.16]

plot '../SF100/20_nekomud/600sEnhancementExperiment/RandoopDefault/coverageProfileData.txt' using ($1/1000):3 title 'Default' with linespoints lw 3, \
'../SF100/20_nekomud/600sEnhancementExperiment/1.ConstantMiner/coverageProfileData.txt' using ($1/1000):3 title 'Constant mining' with linespoints lw 3, \
'../SF100/20_nekomud/600sEnhancementExperiment/4.Impurity/coverageProfileData.txt' using ($1/1000):3 title 'Impurity' with linespoints lw 3, \
'../SF100/20_nekomud/600sEnhancementExperiment/3.ElephantBrain/coverageProfileData.txt' using ($1/1000):3 title 'Elephant brain' with linespoints lw 3, \
'../SF100/20_nekomud/600sEnhancementExperiment/5.Detective/coverageProfileData.txt' using ($1/1000):3 title 'Detective' with linespoints lw 3, \
'../SF100/20_nekomud/600sEnhancementExperiment/2.Orienteering/coverageProfileData.txt' using ($1/1000):3 title 'Orienteering' with linespoints lw 3, \
'../SF100/20_nekomud/600sEnhancementExperiment/6.Bloodhound/coverageProfileData.txt' using ($1/1000):3 title 'Bloodhound' with linespoints lw 3, \
'../SF100/20_nekomud/600sEnhancementExperiment/CombinedEnhancement/coverageProfileData.txt' using ($1/1000):3 title 'Combined' with linespoints lw 3



set out "2_a4j_timel.ps"

set ylabel "Instruction coverage"
set yrange [0.0:0.9]

plot '../SF100/2_a4j/600sEnhancementExperiment/RandoopDefault/coverageProfileData.txt' using ($1/1000):2 title 'Default' with linespoints lw 3, \
'../SF100/2_a4j/600sEnhancementExperiment/1.ConstantMiner/coverageProfileData.txt' using ($1/1000):2 title 'Constant mining' with linespoints lw 3, \
'../SF100/2_a4j/600sEnhancementExperiment/4.Impurity/coverageProfileData.txt' using ($1/1000):2 title 'Impurity' with linespoints lw 3, \
'../SF100/2_a4j/600sEnhancementExperiment/3.ElephantBrain/coverageProfileData.txt' using ($1/1000):2 title 'Elephant brain' with linespoints lw 3, \
'../SF100/2_a4j/600sEnhancementExperiment/5.Detective/coverageProfileData.txt' using ($1/1000):2 title 'Detective' with linespoints lw 3, \
'../SF100/2_a4j/600sEnhancementExperiment/2.Orienteering/coverageProfileData.txt' using ($1/1000):2 title 'Orienteering' with linespoints lw 3, \
'../SF100/2_a4j/600sEnhancementExperiment/6.Bloodhound/coverageProfileData.txt' using ($1/1000):2 title 'Bloodhound' with linespoints lw 3, \
'../SF100/2_a4j/600sEnhancementExperiment/CombinedEnhancement/coverageProfileData.txt' using ($1/1000):2 title 'Combined' with linespoints lw 3



set out "2_a4j_timel_b.ps"

set ylabel "Branch coverage"
set yrange [0.0:0.6]

plot '../SF100/2_a4j/600sEnhancementExperiment/RandoopDefault/coverageProfileData.txt' using ($1/1000):3 title 'Default' with linespoints lw 3, \
'../SF100/2_a4j/600sEnhancementExperiment/1.ConstantMiner/coverageProfileData.txt' using ($1/1000):3 title 'Constant mining' with linespoints lw 3, \
'../SF100/2_a4j/600sEnhancementExperiment/4.Impurity/coverageProfileData.txt' using ($1/1000):3 title 'Impurity' with linespoints lw 3, \
'../SF100/2_a4j/600sEnhancementExperiment/3.ElephantBrain/coverageProfileData.txt' using ($1/1000):3 title 'Elephant brain' with linespoints lw 3, \
'../SF100/2_a4j/600sEnhancementExperiment/5.Detective/coverageProfileData.txt' using ($1/1000):3 title 'Detective' with linespoints lw 3, \
'../SF100/2_a4j/600sEnhancementExperiment/2.Orienteering/coverageProfileData.txt' using ($1/1000):3 title 'Orienteering' with linespoints lw 3, \
'../SF100/2_a4j/600sEnhancementExperiment/6.Bloodhound/coverageProfileData.txt' using ($1/1000):3 title 'Bloodhound' with linespoints lw 3, \
'../SF100/2_a4j/600sEnhancementExperiment/CombinedEnhancement/coverageProfileData.txt' using ($1/1000):3 title 'Combined' with linespoints lw 3



set out "33_javaviewcontrol_timel.ps"

set key center right
set ylabel "Instruction coverage"
set yrange [0.0:0.35]

plot '../SF100/33_javaviewcontrol/600sEnhancementExperiment/RandoopDefault/coverageProfileData.txt' using ($1/1000):2 title 'Default' with linespoints lw 3, \
'../SF100/33_javaviewcontrol/600sEnhancementExperiment/1.ConstantMiner/coverageProfileData.txt' using ($1/1000):2 title 'Constant mining' with linespoints lw 3, \
'../SF100/33_javaviewcontrol/600sEnhancementExperiment/4.Impurity/coverageProfileData.txt' using ($1/1000):2 title 'Impurity' with linespoints lw 3, \
'../SF100/33_javaviewcontrol/600sEnhancementExperiment/3.ElephantBrain/coverageProfileData.txt' using ($1/1000):2 title 'Elephant brain' with linespoints lw 3, \
'../SF100/33_javaviewcontrol/600sEnhancementExperiment/5.Detective/coverageProfileData.txt' using ($1/1000):2 title 'Detective' with linespoints lw 3, \
'../SF100/33_javaviewcontrol/600sEnhancementExperiment/2.Orienteering/coverageProfileData.txt' using ($1/1000):2 title 'Orienteering' with linespoints lw 3, \
'../SF100/33_javaviewcontrol/600sEnhancementExperiment/6.Bloodhound/coverageProfileData.txt' using ($1/1000):2 title 'Bloodhound' with linespoints lw 3, \
'../SF100/33_javaviewcontrol/600sEnhancementExperiment/CombinedEnhancement/coverageProfileData.txt' using ($1/1000):2 title 'Combined' with linespoints lw 3



set out "33_javaviewcontrol_timel_b.ps"

set key bottom right
set key opaque
set ylabel "Branch coverage"
set yrange [0.0:0.14]

plot '../SF100/33_javaviewcontrol/600sEnhancementExperiment/RandoopDefault/coverageProfileData.txt' using ($1/1000):3 title 'Default' with linespoints lw 3, \
'../SF100/33_javaviewcontrol/600sEnhancementExperiment/1.ConstantMiner/coverageProfileData.txt' using ($1/1000):3 title 'Constant mining' with linespoints lw 3, \
'../SF100/33_javaviewcontrol/600sEnhancementExperiment/4.Impurity/coverageProfileData.txt' using ($1/1000):3 title 'Impurity' with linespoints lw 3, \
'../SF100/33_javaviewcontrol/600sEnhancementExperiment/3.ElephantBrain/coverageProfileData.txt' using ($1/1000):3 title 'Elephant brain' with linespoints lw 3, \
'../SF100/33_javaviewcontrol/600sEnhancementExperiment/5.Detective/coverageProfileData.txt' using ($1/1000):3 title 'Detective' with linespoints lw 3, \
'../SF100/33_javaviewcontrol/600sEnhancementExperiment/2.Orienteering/coverageProfileData.txt' using ($1/1000):3 title 'Orienteering' with linespoints lw 3, \
'../SF100/33_javaviewcontrol/600sEnhancementExperiment/6.Bloodhound/coverageProfileData.txt' using ($1/1000):3 title 'Bloodhound' with linespoints lw 3, \
'../SF100/33_javaviewcontrol/600sEnhancementExperiment/CombinedEnhancement/coverageProfileData.txt' using ($1/1000):3 title 'Combined' with linespoints lw 3

set key noopaque


set out "41_follow_timel.ps"

set ylabel "Instruction coverage"
set yrange [0.0:0.3]

plot '../SF100/41_follow/600sEnhancementExperiment/RandoopDefault/coverageProfileData.txt' using ($1/1000):2 title 'Default' with linespoints lw 3, \
'../SF100/41_follow/600sEnhancementExperiment/1.ConstantMiner/coverageProfileData.txt' using ($1/1000):2 title 'Constant mining' with linespoints lw 3, \
'../SF100/41_follow/600sEnhancementExperiment/4.Impurity/coverageProfileData.txt' using ($1/1000):2 title 'Impurity' with linespoints lw 3, \
'../SF100/41_follow/600sEnhancementExperiment/3.ElephantBrain/coverageProfileData.txt' using ($1/1000):2 title 'Elephant brain' with linespoints lw 3, \
'../SF100/41_follow/600sEnhancementExperiment/5.Detective/coverageProfileData.txt' using ($1/1000):2 title 'Detective' with linespoints lw 3, \
'../SF100/41_follow/600sEnhancementExperiment/2.Orienteering/coverageProfileData.txt' using ($1/1000):2 title 'Orienteering' with linespoints lw 3, \
'../SF100/41_follow/600sEnhancementExperiment/6.Bloodhound/coverageProfileData.txt' using ($1/1000):2 title 'Bloodhound' with linespoints lw 3, \
'../SF100/41_follow/600sEnhancementExperiment/CombinedEnhancement/coverageProfileData.txt' using ($1/1000):2 title 'Combined' with linespoints lw 3



set out "41_follow_timel_b.ps"

set ylabel "Branch coverage"
set yrange [0.0:0.3]

plot '../SF100/41_follow/600sEnhancementExperiment/RandoopDefault/coverageProfileData.txt' using ($1/1000):3 title 'Default' with linespoints lw 3, \
'../SF100/41_follow/600sEnhancementExperiment/1.ConstantMiner/coverageProfileData.txt' using ($1/1000):3 title 'Constant mining' with linespoints lw 3, \
'../SF100/41_follow/600sEnhancementExperiment/4.Impurity/coverageProfileData.txt' using ($1/1000):3 title 'Impurity' with linespoints lw 3, \
'../SF100/41_follow/600sEnhancementExperiment/3.ElephantBrain/coverageProfileData.txt' using ($1/1000):3 title 'Elephant brain' with linespoints lw 3, \
'../SF100/41_follow/600sEnhancementExperiment/5.Detective/coverageProfileData.txt' using ($1/1000):3 title 'Detective' with linespoints lw 3, \
'../SF100/41_follow/600sEnhancementExperiment/2.Orienteering/coverageProfileData.txt' using ($1/1000):3 title 'Orienteering' with linespoints lw 3, \
'../SF100/41_follow/600sEnhancementExperiment/6.Bloodhound/coverageProfileData.txt' using ($1/1000):3 title 'Bloodhound' with linespoints lw 3, \
'../SF100/41_follow/600sEnhancementExperiment/CombinedEnhancement/coverageProfileData.txt' using ($1/1000):3 title 'Combined' with linespoints lw 3



set out "45_lotus_timel.ps"

set ylabel "Instruction coverage"
set yrange [0.0:0.7]

plot '../SF100/45_lotus/600sEnhancementExperiment/RandoopDefault/coverageProfileData.txt' using ($1/1000):2 title 'Default' with linespoints lw 3, \
'../SF100/45_lotus/600sEnhancementExperiment/1.ConstantMiner/coverageProfileData.txt' using ($1/1000):2 title 'Constant mining' with linespoints lw 3, \
'../SF100/45_lotus/600sEnhancementExperiment/4.Impurity/coverageProfileData.txt' using ($1/1000):2 title 'Impurity' with linespoints lw 3, \
'../SF100/45_lotus/600sEnhancementExperiment/3.ElephantBrain/coverageProfileData.txt' using ($1/1000):2 title 'Elephant brain' with linespoints lw 3, \
'../SF100/45_lotus/600sEnhancementExperiment/5.Detective/coverageProfileData.txt' using ($1/1000):2 title 'Detective' with linespoints lw 3, \
'../SF100/45_lotus/600sEnhancementExperiment/2.Orienteering/coverageProfileData.txt' using ($1/1000):2 title 'Orienteering' with linespoints lw 3, \
'../SF100/45_lotus/600sEnhancementExperiment/6.Bloodhound/coverageProfileData.txt' using ($1/1000):2 title 'Bloodhound' with linespoints lw 3, \
'../SF100/45_lotus/600sEnhancementExperiment/CombinedEnhancement/coverageProfileData.txt' using ($1/1000):2 title 'Combined' with linespoints lw 3



set out "45_lotus_timel_b.ps"

set ylabel "Branch coverage"
set yrange [0.0:0.5]

plot '../SF100/45_lotus/600sEnhancementExperiment/RandoopDefault/coverageProfileData.txt' using ($1/1000):3 title 'Default' with linespoints lw 3, \
'../SF100/45_lotus/600sEnhancementExperiment/1.ConstantMiner/coverageProfileData.txt' using ($1/1000):3 title 'Constant mining' with linespoints lw 3, \
'../SF100/45_lotus/600sEnhancementExperiment/4.Impurity/coverageProfileData.txt' using ($1/1000):3 title 'Impurity' with linespoints lw 3, \
'../SF100/45_lotus/600sEnhancementExperiment/3.ElephantBrain/coverageProfileData.txt' using ($1/1000):3 title 'Elephant brain' with linespoints lw 3, \
'../SF100/45_lotus/600sEnhancementExperiment/5.Detective/coverageProfileData.txt' using ($1/1000):3 title 'Detective' with linespoints lw 3, \
'../SF100/45_lotus/600sEnhancementExperiment/2.Orienteering/coverageProfileData.txt' using ($1/1000):3 title 'Orienteering' with linespoints lw 3, \
'../SF100/45_lotus/600sEnhancementExperiment/6.Bloodhound/coverageProfileData.txt' using ($1/1000):3 title 'Bloodhound' with linespoints lw 3, \
'../SF100/45_lotus/600sEnhancementExperiment/CombinedEnhancement/coverageProfileData.txt' using ($1/1000):3 title 'Combined' with linespoints lw 3



set out "74_fixsuite_timel.ps"

set ylabel "Instruction coverage"
set yrange [0.0:0.25]

plot '../SF100/74_fixsuite/600sEnhancementExperiment/RandoopDefault/coverageProfileData.txt' using ($1/1000):2 title 'Default' with linespoints lw 3, \
'../SF100/74_fixsuite/600sEnhancementExperiment/1.ConstantMiner/coverageProfileData.txt' using ($1/1000):2 title 'Constant mining' with linespoints lw 3, \
'../SF100/74_fixsuite/600sEnhancementExperiment/4.Impurity/coverageProfileData.txt' using ($1/1000):2 title 'Impurity' with linespoints lw 3, \
'../SF100/74_fixsuite/600sEnhancementExperiment/3.ElephantBrain/coverageProfileData.txt' using ($1/1000):2 title 'Elephant brain' with linespoints lw 3, \
'../SF100/74_fixsuite/600sEnhancementExperiment/5.Detective/coverageProfileData.txt' using ($1/1000):2 title 'Detective' with linespoints lw 3, \
'../SF100/74_fixsuite/600sEnhancementExperiment/2.Orienteering/coverageProfileData.txt' using ($1/1000):2 title 'Orienteering' with linespoints lw 3, \
'../SF100/74_fixsuite/600sEnhancementExperiment/6.Bloodhound/coverageProfileData.txt' using ($1/1000):2 title 'Bloodhound' with linespoints lw 3, \
'../SF100/74_fixsuite/600sEnhancementExperiment/CombinedEnhancement/coverageProfileData.txt' using ($1/1000):2 title 'Combined' with linespoints lw 3



set out "74_fixsuite_timel_b.ps"

set ylabel "Branch coverage"
set yrange [0.0:0.2]

plot '../SF100/74_fixsuite/600sEnhancementExperiment/RandoopDefault/coverageProfileData.txt' using ($1/1000):3 title 'Default' with linespoints lw 3, \
'../SF100/74_fixsuite/600sEnhancementExperiment/1.ConstantMiner/coverageProfileData.txt' using ($1/1000):3 title 'Constant mining' with linespoints lw 3, \
'../SF100/74_fixsuite/600sEnhancementExperiment/4.Impurity/coverageProfileData.txt' using ($1/1000):3 title 'Impurity' with linespoints lw 3, \
'../SF100/74_fixsuite/600sEnhancementExperiment/3.ElephantBrain/coverageProfileData.txt' using ($1/1000):3 title 'Elephant brain' with linespoints lw 3, \
'../SF100/74_fixsuite/600sEnhancementExperiment/5.Detective/coverageProfileData.txt' using ($1/1000):3 title 'Detective' with linespoints lw 3, \
'../SF100/74_fixsuite/600sEnhancementExperiment/2.Orienteering/coverageProfileData.txt' using ($1/1000):3 title 'Orienteering' with linespoints lw 3, \
'../SF100/74_fixsuite/600sEnhancementExperiment/6.Bloodhound/coverageProfileData.txt' using ($1/1000):3 title 'Bloodhound' with linespoints lw 3, \
'../SF100/74_fixsuite/600sEnhancementExperiment/CombinedEnhancement/coverageProfileData.txt' using ($1/1000):3 title 'Combined' with linespoints lw 3



set out "80_wheelwebtool_timel.ps"

set ylabel "Instruction coverage"
set yrange [0.0:0.25]

plot '../SF100/80_wheelwebtool/600sEnhancementExperiment/RandoopDefault/coverageProfileData.txt' using ($1/1000):2 title 'Default' with linespoints lw 3, \
'../SF100/80_wheelwebtool/600sEnhancementExperiment/1.ConstantMiner/coverageProfileData.txt' using ($1/1000):2 title 'Constant mining' with linespoints lw 3, \
'../SF100/80_wheelwebtool/600sEnhancementExperiment/4.Impurity/coverageProfileData.txt' using ($1/1000):2 title 'Impurity' with linespoints lw 3, \
'../SF100/80_wheelwebtool/600sEnhancementExperiment/3.ElephantBrain/coverageProfileData.txt' using ($1/1000):2 title 'Elephant brain' with linespoints lw 3, \
'../SF100/80_wheelwebtool/600sEnhancementExperiment/5.Detective/coverageProfileData.txt' using ($1/1000):2 title 'Detective' with linespoints lw 3, \
'../SF100/80_wheelwebtool/600sEnhancementExperiment/2.Orienteering/coverageProfileData.txt' using ($1/1000):2 title 'Orienteering' with linespoints lw 3, \
'../SF100/80_wheelwebtool/600sEnhancementExperiment/6.Bloodhound/coverageProfileData.txt' using ($1/1000):2 title 'Bloodhound' with linespoints lw 3, \
'../SF100/80_wheelwebtool/600sEnhancementExperiment/CombinedEnhancement_DemandDriven=off/coverageProfileData.txt' using ($1/1000):2 title 'Combined' with linespoints lw 3



set out "80_wheelwebtool_timel_b.ps"

set ylabel "Branch coverage"
set yrange [0.0:0.25]

plot '../SF100/80_wheelwebtool/600sEnhancementExperiment/RandoopDefault/coverageProfileData.txt' using ($1/1000):3 title 'Default' with linespoints lw 3, \
'../SF100/80_wheelwebtool/600sEnhancementExperiment/1.ConstantMiner/coverageProfileData.txt' using ($1/1000):3 title 'Constant mining' with linespoints lw 3, \
'../SF100/80_wheelwebtool/600sEnhancementExperiment/4.Impurity/coverageProfileData.txt' using ($1/1000):3 title 'Impurity' with linespoints lw 3, \
'../SF100/80_wheelwebtool/600sEnhancementExperiment/3.ElephantBrain/coverageProfileData.txt' using ($1/1000):3 title 'Elephant brain' with linespoints lw 3, \
'../SF100/80_wheelwebtool/600sEnhancementExperiment/5.Detective/coverageProfileData.txt' using ($1/1000):3 title 'Detective' with linespoints lw 3, \
'../SF100/80_wheelwebtool/600sEnhancementExperiment/2.Orienteering/coverageProfileData.txt' using ($1/1000):3 title 'Orienteering' with linespoints lw 3, \
'../SF100/80_wheelwebtool/600sEnhancementExperiment/6.Bloodhound/coverageProfileData.txt' using ($1/1000):3 title 'Bloodhound' with linespoints lw 3, \
'../SF100/80_wheelwebtool/600sEnhancementExperiment/CombinedEnhancement_DemandDriven=off/coverageProfileData.txt' using ($1/1000):3 title 'Combined' with linespoints lw 3



set out "90_dcparseargs_timel.ps"

set ylabel "Instruction coverage"
set yrange [0.0:0.6]

plot '../SF100/90_dcparseargs/600sEnhancementExperiment/RandoopDefault/coverageProfileData.txt' using ($1/1000):2 title 'Default' with linespoints lw 3, \
'../SF100/90_dcparseargs/600sEnhancementExperiment/1.ConstantMiner/coverageProfileData.txt' using ($1/1000):2 title 'Constant mining' with linespoints lw 3, \
'../SF100/90_dcparseargs/600sEnhancementExperiment/4.Impurity/coverageProfileData.txt' using ($1/1000):2 title 'Impurity' with linespoints lw 3, \
'../SF100/90_dcparseargs/600sEnhancementExperiment/3.ElephantBrain/coverageProfileData.txt' using ($1/1000):2 title 'Elephant brain' with linespoints lw 3, \
'../SF100/90_dcparseargs/600sEnhancementExperiment/5.Detective/coverageProfileData.txt' using ($1/1000):2 title 'Detective' with linespoints lw 3, \
'../SF100/90_dcparseargs/600sEnhancementExperiment/2.Orienteering/coverageProfileData.txt' using ($1/1000):2 title 'Orienteering' with linespoints lw 3, \
'../SF100/90_dcparseargs/600sEnhancementExperiment/6.Bloodhound/coverageProfileData.txt' using ($1/1000):2 title 'Bloodhound' with linespoints lw 3, \
'../SF100/90_dcparseargs/600sEnhancementExperiment/CombinedEnhancement/coverageProfileData.txt' using ($1/1000):2 title 'Combined' with linespoints lw 3



set out "90_dcparseargs_timel_b.ps"

set ylabel "Branch coverage"
set yrange [0.0:0.7]

plot '../SF100/90_dcparseargs/600sEnhancementExperiment/RandoopDefault/coverageProfileData.txt' using ($1/1000):3 title 'Default' with linespoints lw 3, \
'../SF100/90_dcparseargs/600sEnhancementExperiment/1.ConstantMiner/coverageProfileData.txt' using ($1/1000):3 title 'Constant mining' with linespoints lw 3, \
'../SF100/90_dcparseargs/600sEnhancementExperiment/4.Impurity/coverageProfileData.txt' using ($1/1000):3 title 'Impurity' with linespoints lw 3, \
'../SF100/90_dcparseargs/600sEnhancementExperiment/3.ElephantBrain/coverageProfileData.txt' using ($1/1000):3 title 'Elephant brain' with linespoints lw 3, \
'../SF100/90_dcparseargs/600sEnhancementExperiment/5.Detective/coverageProfileData.txt' using ($1/1000):3 title 'Detective' with linespoints lw 3, \
'../SF100/90_dcparseargs/600sEnhancementExperiment/2.Orienteering/coverageProfileData.txt' using ($1/1000):3 title 'Orienteering' with linespoints lw 3, \
'../SF100/90_dcparseargs/600sEnhancementExperiment/6.Bloodhound/coverageProfileData.txt' using ($1/1000):3 title 'Bloodhound' with linespoints lw 3, \
'../SF100/90_dcparseargs/600sEnhancementExperiment/CombinedEnhancement/coverageProfileData.txt' using ($1/1000):3 title 'Combined' with linespoints lw 3



set out "91_classviewer_timel.ps"

set ylabel "Instruction coverage"
set yrange [0.0:0.5]

plot '../SF100/91_classviewer/600sEnhancementExperiment/RandoopDefault/coverageProfileData.txt' using ($1/1000):2 title 'Default' with linespoints lw 3, \
'../SF100/91_classviewer/600sEnhancementExperiment/1.ConstantMiner/coverageProfileData.txt' using ($1/1000):2 title 'Constant mining' with linespoints lw 3, \
'../SF100/91_classviewer/600sEnhancementExperiment/4.Impurity/coverageProfileData.txt' using ($1/1000):2 title 'Impurity' with linespoints lw 3, \
'../SF100/91_classviewer/600sEnhancementExperiment/3.ElephantBrain/coverageProfileData.txt' using ($1/1000):2 title 'Elephant brain' with linespoints lw 3, \
'../SF100/91_classviewer/600sEnhancementExperiment/5.Detective/coverageProfileData.txt' using ($1/1000):2 title 'Detective' with linespoints lw 3, \
'../SF100/91_classviewer/600sEnhancementExperiment/2.Orienteering/coverageProfileData.txt' using ($1/1000):2 title 'Orienteering' with linespoints lw 3, \
'../SF100/91_classviewer/600sEnhancementExperiment/6.Bloodhound/coverageProfileData.txt' using ($1/1000):2 title 'Bloodhound' with linespoints lw 3, \
'../SF100/91_classviewer/600sEnhancementExperiment/CombinedEnhancement/coverageProfileData.txt' using ($1/1000):2 title 'Combined' with linespoints lw 3



set out "91_classviewer_timel_b.ps"

set ylabel "Branch coverage"
set yrange [0.0:0.35]

plot '../SF100/91_classviewer/600sEnhancementExperiment/RandoopDefault/coverageProfileData.txt' using ($1/1000):3 title 'Default' with linespoints lw 3, \
'../SF100/91_classviewer/600sEnhancementExperiment/1.ConstantMiner/coverageProfileData.txt' using ($1/1000):3 title 'Constant mining' with linespoints lw 3, \
'../SF100/91_classviewer/600sEnhancementExperiment/4.Impurity/coverageProfileData.txt' using ($1/1000):3 title 'Impurity' with linespoints lw 3, \
'../SF100/91_classviewer/600sEnhancementExperiment/3.ElephantBrain/coverageProfileData.txt' using ($1/1000):3 title 'Elephant brain' with linespoints lw 3, \
'../SF100/91_classviewer/600sEnhancementExperiment/5.Detective/coverageProfileData.txt' using ($1/1000):3 title 'Detective' with linespoints lw 3, \
'../SF100/91_classviewer/600sEnhancementExperiment/2.Orienteering/coverageProfileData.txt' using ($1/1000):3 title 'Orienteering' with linespoints lw 3, \
'../SF100/91_classviewer/600sEnhancementExperiment/6.Bloodhound/coverageProfileData.txt' using ($1/1000):3 title 'Bloodhound' with linespoints lw 3, \
'../SF100/91_classviewer/600sEnhancementExperiment/CombinedEnhancement/coverageProfileData.txt' using ($1/1000):3 title 'Combined' with linespoints lw 3

set out "scch_timel.ps"

set ylabel "Instruction coverage"
set yrange [0.0:0.7]

plot '../scch/600sEnhancementExperiment/RandoopDefault/coverageProfileData.txt' using ($1/1000):2 title 'Default' with linespoints lw 3, \
'../scch/600sEnhancementExperiment/1.ConstantMiner/coverageProfileData.txt' using ($1/1000):2 title 'Constant mining' with linespoints lw 3, \
'../scch/600sEnhancementExperiment/4.Impurity/coverageProfileData.txt' using ($1/1000):2 title 'Impurity' with linespoints lw 3, \
'../scch/600sEnhancementExperiment/3.ElephantBrain/coverageProfileData.txt' using ($1/1000):2 title 'Elephant brain' with linespoints lw 3, \
'../scch/600sEnhancementExperiment/5.Detective/coverageProfileData.txt' using ($1/1000):2 title 'Detective' with linespoints lw 3, \
'../scch/600sEnhancementExperiment/2.Orienteering/coverageProfileData.txt' using ($1/1000):2 title 'Orienteering' with linespoints lw 3, \
'../scch/600sEnhancementExperiment/6.Bloodhound/coverageProfileData.txt' using ($1/1000):2 title 'Bloodhound' with linespoints lw 3, \
'../scch/600sEnhancementExperiment/CombinedEnhancement/coverageProfileData.txt' using ($1/1000):2 title 'Combined' with linespoints lw 3



set out "scch_timel_b.ps"

set ylabel "Branch coverage"
set yrange [0.0:0.6]

plot '../scch/600sEnhancementExperiment/RandoopDefault/coverageProfileData.txt' using ($1/1000):3 title 'Default' with linespoints lw 3, \
'../scch/600sEnhancementExperiment/1.ConstantMiner/coverageProfileData.txt' using ($1/1000):3 title 'Constant mining' with linespoints lw 3, \
'../scch/600sEnhancementExperiment/4.Impurity/coverageProfileData.txt' using ($1/1000):3 title 'Impurity' with linespoints lw 3, \
'../scch/600sEnhancementExperiment/3.ElephantBrain/coverageProfileData.txt' using ($1/1000):3 title 'Elephant brain' with linespoints lw 3, \
'../scch/600sEnhancementExperiment/5.Detective/coverageProfileData.txt' using ($1/1000):3 title 'Detective' with linespoints lw 3, \
'../scch/600sEnhancementExperiment/2.Orienteering/coverageProfileData.txt' using ($1/1000):3 title 'Orienteering' with linespoints lw 3, \
'../scch/600sEnhancementExperiment/6.Bloodhound/coverageProfileData.txt' using ($1/1000):3 title 'Bloodhound' with linespoints lw 3, \
'../scch/600sEnhancementExperiment/CombinedEnhancement/coverageProfileData.txt' using ($1/1000):3 title 'Combined' with linespoints lw 3
