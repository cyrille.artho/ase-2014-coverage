#!/bin/sh
# generate index HTML page

cat <<EOH
<html>
<head>
<title>Plots for ASE 2014 paper</title>
</head>
<body>
<h1>
Time-line plots
</h1>
<table cellpadding="3">
<tr>
<th>Benchmark</th>
<th colspan="2">Plots</th>
</tr>
EOH

for F in *timel.pdf
do
	B="`echo $F | sed -e 's/_timel.pdf//'`"
	echo '<tr><td>'$B'</td>'
	echo '<td><a href="'$F'">Instruction coverage</a></td>'
	echo '<td><a href="'$B'_timel_b.pdf">Branch coverage</a></td>'
	echo '</tr>'
done

cat <<EOF
</table>
</body>
</html>
EOF
